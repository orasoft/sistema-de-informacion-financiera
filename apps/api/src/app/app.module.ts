import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OrmModule } from '@sif/module-api/orm';
import { AdminCompanyModule } from '@sif/module-api/discovery/admin/companies';
import { AdminBranchModule } from '@sif/module-api/discovery/admin/branches';
import { AdminUserModule } from '@sif/module-api/discovery/admin/users';
import { AdminRoleModule } from '@sif/module-api/discovery/admin/roles';
import { AuthLoginModule } from '@sif/module-api/discovery/auth/login';
import { AdminUserRoleBranchModule } from '../../../../libs/module-api/discovery/admin/user-role-branch/src/lib/admin-user-role-branch.module';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    MulterModule.register({
      dest: './files',
    }),
    OrmModule,
    AdminCompanyModule,
    AdminBranchModule,
    AdminUserModule,
    AdminRoleModule,
    AuthLoginModule,
    AdminUserRoleBranchModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
