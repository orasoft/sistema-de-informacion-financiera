import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';
import { AppComponent } from './app.component';
import { fuseConfig, FuseModule } from '@sif/module/ui/theme/fuse';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@sif/module/ui/theme/components';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import { LayoutModule } from '@sif/module/ui/theme/layout';
import { LoginModule } from '@sif/module/auth/login';
import { CompanySelectionModule } from '@sif/module/auth/company-selection';
import { BranchSelectionModule } from '@sif/module/auth/branch-selection';
import { RoleSelectionModule } from '@sif/module/auth/role-selection';
import { DashboardModule } from '@sif/module/dashboard/dashboard';
import { AdminCompaniesModule } from '@sif/module/admin/companies';
import { AdminUsersModule } from '@sif/module/admin/users';
import { AdminBranchesModule } from '@sif/module/admin/branches';
import { UIAppSharedModule } from '@sif/module/ui/app/shared';
import { AdminRolesModule } from '@sif/module/admin/roles';
import { AdminAssignRoleModule } from '@sif/module/admin/assign-role';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { AuthProfileModule } from '../../../../libs/module/auth/profile/src/lib/auth-profile.module';



const appRoutes: Routes = [
  {
    path: '**',
    redirectTo: 'auth/login'
  }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),

    TranslateModule.forRoot(),

    // Material moment date module
    MatMomentDateModule,

    // Material
    MatButtonModule,
    MatIconModule,

    // Fuse modules
    FuseModule.forRoot(fuseConfig),
    FuseProgressBarModule,
    FuseSharedModule,
    FuseSidebarModule,
    FuseThemeOptionsModule,

    // App modules
    LayoutModule,

    // SIF Module
    LoginModule,
    CompanySelectionModule,
    BranchSelectionModule,
    RoleSelectionModule,
    DashboardModule,
    AuthProfileModule,
    // SIF UI
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    // SIF ADMIN
    AdminUsersModule,
    AdminCompaniesModule,
    AdminBranchesModule,
    AdminRolesModule,
    AdminAssignRoleModule,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
