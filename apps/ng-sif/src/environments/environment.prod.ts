export const environment = {
  production: true,
  hmr: true,
  apiUrl: 'http://ec2-3-16-94-191.us-east-2.compute.amazonaws.com:3333/api/v1/',
  version: 'V1.0.1-PROD'
};
