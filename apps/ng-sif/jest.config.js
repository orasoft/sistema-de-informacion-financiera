module.exports = {
  name: 'ng-sif',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/ng-sif',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
