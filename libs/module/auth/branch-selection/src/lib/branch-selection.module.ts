import { NgModule } from '@angular/core';
import { BranchSelectionComponent } from './components/branch-selection.component';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import { MatIconModule } from '@angular/material';

const routes = [
  {
    path: 'auth/branch-selection',
    component: BranchSelectionComponent
  }
];

@NgModule({
  declarations: [
    BranchSelectionComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatIconModule
  ],
  providers: [],
  exports: []
})
export class BranchSelectionModule {
}
