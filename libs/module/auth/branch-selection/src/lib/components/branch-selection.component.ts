import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations, FuseConfigService } from '@sif/module/ui/theme/fuse';
import { AuthLoginService } from '../../../../login/src/lib/services/auth-login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-branch-selection',
  templateUrl: './branch-selection.component.html',
  styleUrls: ['./branch-selection.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class BranchSelectionComponent implements OnInit {

  public branches = [];
  public infoCompany;

  constructor(private _fuseConfigService: FuseConfigService,
              private loginService: AuthLoginService,
              private router: Router) {
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
    this.infoCompany = JSON.parse(localStorage.getItem('infoCompany'));
  }

  ngOnInit(): void {
    const branches = [];
    this.loginService.me(localStorage.getItem('idUser')).subscribe(data => {
      data.branches.forEach(resp => {
        this.loginService.meCompany(resp.id).subscribe(dataComp => {
          if (this.infoCompany.id === dataComp.company.id) {
            branches.push( {
             id: dataComp.id,
             name: dataComp.name,
            })
          }
        });
      });
      this.branches = branches;
    });
  }

  branchSelected(branch) {
    console.log(branch);
    localStorage.setItem('infoBranch', JSON.stringify({id: branch.id, name: branch.name}));
    this.router.navigate(['/auth/role-selection']);
  }
}
