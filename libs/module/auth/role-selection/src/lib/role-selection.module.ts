import { NgModule } from '@angular/core';
import { RoleSelectionComponent } from './components/role-selection.component';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import { MatIconModule } from '@angular/material';

const routes = [
  {
    path: 'auth/role-selection',
    component: RoleSelectionComponent
  }
];

@NgModule({
  declarations: [
    RoleSelectionComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatIconModule
  ],
  providers: [],
  exports: []
})
export class RoleSelectionModule {
}
