import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations, FuseConfigService } from '@sif/module/ui/theme/fuse';
import { AuthLoginService } from '../../../../login/src/lib/services/auth-login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-role-selection',
  templateUrl: './role-selection.component.html',
  styleUrls: ['./role-selection.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class RoleSelectionComponent implements OnInit {

  public roles = [];
  public infoBranch;
  constructor(private _fuseConfigService: FuseConfigService,
              private loginService: AuthLoginService,
              private router: Router) {
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
    this.infoBranch = JSON.parse(localStorage.getItem('infoBranch'));
  }

  ngOnInit(): void {
    const roles = [];
    this.loginService.me(localStorage.getItem('idUser')).subscribe(data => {
      data.roles.forEach( resp => {
        if(this.infoBranch.id === resp.branchId) {
          roles.push({
            id: resp.id,
            name: resp.name
          })
        }
      });
      this.roles = roles;
    })
  }

  roleSelected(role) {
    localStorage.setItem('infoRole', JSON.stringify({id: role.id, name: role.name}));
    this.router.navigate(['/dashboard']);
  }
}
