import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations, FuseConfigService } from '@sif/module/ui/theme/fuse';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { AuthLoginService } from '../services/auth-login.service';
import { ToastrService } from 'ngx-toastr';
import { AuthProfileService } from '../../../../profile/src/lib/services/auth-profile.service';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private _fuseConfigService: FuseConfigService,
              private _formBuilder: FormBuilder,
              private _snackBar: MatSnackBar,
              private _router: Router,
              private _authLoginService: AuthLoginService,
              private authProfileService: AuthProfileService,
              private toastr: ToastrService,
              ) {
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {

      const user = {
        username: this.loginForm.getRawValue().user,
        password: this.loginForm.getRawValue().password,
      };
      this._authLoginService.login(user).subscribe( resp => {
        console.log(resp);
        if (resp.header.code === 200) {
          localStorage.setItem('token', resp.data.access_token);
          localStorage.setItem('idUser', resp.data.idUser);
          this.authProfileService.getAvatar(localStorage.getItem('idUser')).subscribe( avatar => {
            this.createImageFromBlob(avatar);
          });
          this._router.navigate(['/auth/company-selection']);
          this.toastr.success('Inicio de Sesión Exitoso');
        }else {
          this.toastr.warning('Usuario/Contraseña incorrectos');
        }
      });
    }
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load',
      () => {
        const picture: any = reader.result;
        localStorage.setItem('avatar', picture);
      },
      false);

    if (image) {
      if (image.type !== 'application/pdf')
        reader.readAsDataURL(image);
    }
  }
}
