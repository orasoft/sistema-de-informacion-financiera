import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RequestAuthLogin } from '@sif/shared/message/request';
import { ResponseAuthLogin } from '@sif/shared/message/response';
// tslint:disable-next-line:nx-enforce-module-boundaries
import { environment } from '../../../../../../../apps/ng-sif/src/environments/environment';

@Injectable()
export class AuthLoginService {

  private url = environment.apiUrl;

  constructor(private _httpClient: HttpClient) {}

  public login(user: Partial<RequestAuthLogin>): Observable<ResponseAuthLogin> {
    return this._httpClient.post<ResponseAuthLogin>(`${this.url}auth/login`, user);
  }

  public me(idUser): Observable<any> {
    return this._httpClient.get<any>(`${this.url}auth/login/me/${idUser}`);
  }

  public meCompany(idBranch): Observable<any> {
    return this._httpClient.get<any>(`${this.url}auth/login/company/${idBranch}`)
  }

}
