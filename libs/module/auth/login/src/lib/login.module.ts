import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatSnackBarModule
} from '@angular/material';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import { LoginComponent } from './components/login.component';
import { AuthLoginService } from './services/auth-login.service';

const routes = [
  {
    path: 'auth/login',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    FuseSharedModule,
    MatSnackBarModule
  ],
  providers: [
    AuthLoginService
  ]
})
export class LoginModule {
}
