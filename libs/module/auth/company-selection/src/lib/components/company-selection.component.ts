import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations, FuseConfigService } from '@sif/module/ui/theme/fuse';
// tslint:disable-next-line:nx-enforce-module-boundaries
import { AuthLoginService } from '../../../../login/src/lib/services/auth-login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-company-selection',
  templateUrl: './company-selection.component.html',
  styleUrls: ['./company-selection.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class CompanySelectionComponent implements OnInit {

  public companies = [];

  constructor(private _fuseConfigService: FuseConfigService,
              private loginService: AuthLoginService,
              private toastr: ToastrService,
              private router: Router) {
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  ngOnInit(): void {

    this.loginService.me(localStorage.getItem('idUser')).subscribe( data =>  {
      if (data.branches.length > 0) {
        const user = {
          name: data.user.name,
          username: data.user.username,
          email: data.user.email
        };
        localStorage.setItem('infoUser', JSON.stringify(user));
        const companies = [];

        data.branches.forEach( resp =>  {
          this.loginService.meCompany(resp.id).subscribe( dataComp => {

            let exist = false;
            if (companies.length === 0) {
              companies.push({
                id: dataComp.company.id,
                name: dataComp.company.name,
                idBranch: dataComp.id
              })
            } else {
              companies.forEach( line => {
                if(line.id === dataComp.company.id) {
                  exist = true;
                }
              });
              if (!exist) {
                companies.push({
                  id: dataComp.company.id,
                  name: dataComp.company.name,
                  idBranch: dataComp.id
                })
              }
            }
          })
        });
        this.companies = companies;
      } else {
        this.toastr.warning('Estimado usuario por el momento no tiene roles asignados, contacte al administrador');
        this.router.navigate(['/auth/login']);
      }
    });
  }

  companySelected(company) {
    console.log(company);
    localStorage.setItem('infoCompany', JSON.stringify({id: company.id, name: company.name}));
    this.router.navigate(['/auth/branch-selection'])
  }
}
