import { NgModule } from '@angular/core';
import { CompanySelectionComponent } from './components/company-selection.component';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import { MatIconModule } from '@angular/material';

const routes = [
  {
    path: 'auth/company-selection',
    component: CompanySelectionComponent
  }
];

@NgModule({
  declarations: [
    CompanySelectionComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatIconModule,
  ],
  providers: [],
  exports: []
})
export class CompanySelectionModule {
}
