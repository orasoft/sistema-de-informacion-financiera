import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// tslint:disable-next-line:nx-enforce-module-boundaries
import { environment } from '../../../../../../../apps/ng-sif/src/environments/environment';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
// tslint:disable-next-line:nx-enforce-module-boundaries
import { ResponseAdminUser } from '@sif/shared/message/response';


@Injectable()
export class AuthProfileService {

  private url = environment.apiUrl;

  constructor(private http: HttpClient,
              private spinner: NgxSpinnerService) {}

  public updateAvatar(userId, file): Observable<any>{
    this.spinner.show();
   return this.http.post<any>(`${this.url}admin/user/${userId}/avatar`, file).pipe(finalize(() => {
     this.hideSpinner();
   }));
  }

  public getAvatar(userId): Observable<Blob>{
    this.spinner.show();
    return this.http.get(`${this.url}admin/user/avatars/${userId}`, { responseType: "blob" }).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }

  public changePass(userId, body): Observable<ResponseAdminUser> {
    this.spinner.show();
    return this.http.put<ResponseAdminUser>(`${this.url}admin/user/changePass/${userId}`, body).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }

  public hideSpinner() {
    setTimeout(() =>
        this.spinner.hide(),
      1000);
  }

}
