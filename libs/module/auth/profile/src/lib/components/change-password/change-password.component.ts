import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthProfileService } from '../../services/auth-profile.service';
import { IUser } from '@sif/module-api/orm';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup;
  userInfo: IUser;

  constructor(
    private _formBuilder: FormBuilder,
    private toastr: ToastrService,
    private authProfileService: AuthProfileService,
    private _dialogRef: MatDialogRef<ChangePasswordComponent>,
  ){}

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      currentPass: ['', Validators.required],
      newPass: ['', Validators.required],
      confirmPass: ['', Validators.required],
    })
  }

  onSubmit() {
    if (this.form.valid) {
      if (this.form.getRawValue().newPass === this.form.getRawValue().confirmPass) {
        const body = {
          currentPass: this.form.getRawValue().currentPass,
          newPass: this.form.getRawValue().newPass,
        }
        this.authProfileService.changePass(localStorage.getItem('idUser'), body).subscribe( response => {
          if (response.header.code === 200) {
            this._dialogRef.close();
            this.toastr.success('La clave fue actualizada con éxito');
          } else {
            this.toastr.error('No fue posible actualizar la clave')
          }
        })
      } else {
        this.toastr.warning('La nueva contraseña no coincide');
      }
    } else {
      this.toastr.warning('Completar los campos requeridos');
    }
  }

}
