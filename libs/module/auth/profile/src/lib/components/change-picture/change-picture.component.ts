import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { AuthProfileService } from '../../services/auth-profile.service';
import { FileValidator } from 'ngx-material-file-input';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-change-picture',
  templateUrl: './change-picture.component.html',
  styleUrls: ['./change-picture.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ChangePictureComponent implements OnInit {

  form: FormGroup;
  formDoc: FormGroup;
  fileData: File = null;
  public picture: any = 'assets/images/avatars/profile.jpg';

  maxSize = 1000000;

  constructor(private _fb: FormBuilder,
              private _dialogRef: MatDialogRef<ChangePictureComponent>,
              private http: AuthProfileService
  ) {
    if (localStorage.getItem('avatar')) {
      this.picture = localStorage.getItem('avatar');
    } else {
      this.picture = 'assets/images/avatars/profile.jpg';
    }
  }

  ngOnInit(): void {
    this.formDoc = this._fb.group({
      requiredfile: [
        undefined,
        [Validators.required, FileValidator.maxContentSize(this.maxSize)]
      ]
    });
  }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  preview() {
    // Show preview
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.picture = reader.result;
    };
  }

  onSubmit() {
    if (this.formDoc.valid) {
      const formData = new FormData();
      formData.append('file', this.fileData);
      this.http.updateAvatar(localStorage.getItem('idUser'), formData).subscribe(resp => {
        this.http.getAvatar(localStorage.getItem('idUser')).subscribe((image: any) => {
          this.createImageFromBlob(image);
        });
      });
    }
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load',
      () => {
        this.picture = reader.result;
        localStorage.setItem('avatar', this.picture);
        setTimeout(function() {
          window.location.reload();
        }, 1000);
      },
      false);

    if (image) {
      if (image.type !== 'application/pdf')
        reader.readAsDataURL(image);
    }
  }

}
