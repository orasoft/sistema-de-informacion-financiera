import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ChangePictureComponent } from './change-picture/change-picture.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ProfileComponent {

  public infouser;
  public picture = 'assets/images/avatars/profile.jpg';

  /**
   * Constructor
   */
  constructor(
    public dialog: MatDialog
  ) {
    this.infouser = JSON.parse(localStorage.getItem('infoUser'));

    if (localStorage.getItem('avatar')) {
      this.picture = localStorage.getItem('avatar');
    } else {
      this.picture = 'assets/images/avatars/profile.jpg';
    }

  }

  pictureModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.hasBackdrop = true;
    this.dialog.open(ChangePictureComponent, dialogConfig);
  }

  passModal() {
    const dialogConfig =  new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.hasBackdrop = true;
    this.dialog.open(ChangePasswordComponent, dialogConfig);
  }
}
