import { NgModule } from '@angular/core';
import { ProfileComponent } from './components/profile.component';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatDialogModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule
} from '@angular/material';
import { FlexModule } from '@angular/flex-layout';
import { ChangePictureComponent } from './components/change-picture/change-picture.component';
import { AuthProfileService } from './services/auth-profile.service';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

const routes = [
  {
    path     : 'auth/profile',
    component: ProfileComponent,
  }
];

@NgModule({
  declarations: [
    ProfileComponent,
    ChangePictureComponent,
    ChangePasswordComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    FlexModule,
    MatDialogModule,
    MaterialFileInputModule,
    MatFormFieldModule,
    CommonModule,
    ReactiveFormsModule,
    MatInputModule
  ],
  providers   : [
    AuthProfileService
  ],
  entryComponents: [
    ChangePictureComponent,
    ChangePasswordComponent
  ]
})

export class AuthProfileModule {
}
