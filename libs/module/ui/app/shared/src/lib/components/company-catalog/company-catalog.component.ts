import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import { CatalogService } from '../../services/catalog.service';
import { AdminCompanyCatalog } from '@sif/shared/message/response';

@Component({
  selector: 'app-company-catalog',
  templateUrl: 'company-catalog.component.html',
  styleUrls: ['company-catalog.component.scss'],
})
export class CompanyCatalogComponent implements OnInit, OnChanges {

  @Output() itemSelected$ = new EventEmitter<string>();

  @Input() inputCompanyId?: string;

  public itemSelected: Partial<AdminCompanyCatalog>;

  public items: Partial<AdminCompanyCatalog>[] = [];

  private _isFirstChange = false;

  constructor(private readonly _catalogService: CatalogService) {}

  ngOnInit(): void {
    console.log(`ngOnInit ....`);
    this._catalogService.getCompanies().subscribe(data => {
      this.items = data.catalog;
      if (this._isFirstChange) {
        this.itemSelected = this.searchItem()[0];
        console.log(`Primera seleccionado => ${JSON.stringify(this.itemSelected)}`);
      } else {
        this.itemSelected = this.items[0];
        console.log(`Default => ${JSON.stringify(this.itemSelected)}`);
      }
      this._notification();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(`ngOnChanges => ${JSON.stringify(changes)}`);
    if (changes.inputCompanyId && changes.inputCompanyId.isFirstChange()) {
      this._isFirstChange = true;
    }
    this._notification();
  }

  valueChange() {
    this._notification();
  }

  private _notification() {
    setTimeout(() => {
      this.itemSelected$.emit(this.itemSelected.id);
    }, 500);
  }

  private searchItem() {
    return this.items.filter((data) => {
      return data.id === this.inputCompanyId;
    })
  }

}
