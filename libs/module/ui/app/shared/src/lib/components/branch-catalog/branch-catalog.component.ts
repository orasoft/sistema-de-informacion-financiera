import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { CatalogService } from '../../services/catalog.service';
import { AdminBranchCatalog } from '@sif/shared/message/response';


@Component({
  selector: 'app-branch-catalog',
  templateUrl: 'branch-catalog.component.html',
  styleUrls: ['branch-catalog.component.scss']
})
export class BranchCatalogComponent implements OnInit, OnChanges {

  @Output() itemSelected$ = new EventEmitter<string>();

  @Input() inputBranchId?: string;

  @Input() inputCompanyId?: string;

  public itemSelected: Partial<AdminBranchCatalog>;

  public items: Partial<AdminBranchCatalog>[] = [];

  private _isFirstChange = false;

  constructor(private readonly _catalogService: CatalogService) {
  }

  ngOnInit(): void {
    this.getBranches();
  }

  getBranches() {
    if (this.inputCompanyId !== null && this.inputCompanyId !== undefined) {
      this._catalogService.getBranchesById(this.inputCompanyId).subscribe(data => {
        this.items = data.catalog;
        this.itemSelected = this.items[0];
        this._notification();
      });
    } else {
      this._catalogService.getBranches().subscribe(data => {
        this.items = data.catalog;
        this.itemSelected = this.items[0];
        this._notification();
      });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(`ngOnChanges => ${JSON.stringify(changes)}`);
    this.getBranches();
    if (changes.inputCompanyId && changes.inputCompanyId.isFirstChange()) {
      this._isFirstChange = true;
    }
    this._notification();
  }

  valueChange() {
    this._notification();
  }

  private _notification() {
    setTimeout(() => {
      if (this.itemSelected !== undefined  && this.itemSelected !== null) {
      this.itemSelected$.emit(this.itemSelected.id);
      }
    }, 1000);
  }

  private searchItem() {
    return this.items.filter((data) => {
      return data.id === this.inputBranchId;
    });
  }

}
