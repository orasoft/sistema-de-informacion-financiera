import { NgModule } from '@angular/core';
import { CompanyCatalogComponent } from './components/company-catalog/company-catalog.component';
import { MatFormFieldModule, MatOptionModule, MatSelectModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CatalogService } from './services/catalog.service';
import { HttpClientModule } from '@angular/common/http';
import { BranchCatalogComponent } from './components/branch-catalog/branch-catalog.component';


@NgModule({
  imports: [
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    FormsModule,
    CommonModule,
    HttpClientModule
  ],
  exports: [
    CompanyCatalogComponent,
    BranchCatalogComponent
  ],
  declarations: [
    CompanyCatalogComponent,
    BranchCatalogComponent
  ],
  providers: [
    CatalogService
  ]
})
export class UIAppSharedModule {

}
