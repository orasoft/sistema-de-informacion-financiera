import { HttpClient } from '@angular/common/http';
import { ResponseAdminBranchCatalog, ResponseAdminCompanyCatalog } from '@sif/shared/message/response';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../../../../apps/ng-sif/src/environments/environment';

@Injectable()
export class CatalogService {

  private url = environment.apiUrl;

  constructor(private readonly _http: HttpClient) {
  }

  getCompanies(): Observable<ResponseAdminCompanyCatalog> {
    return this._http.get<ResponseAdminCompanyCatalog>(`${this.url}admin/company/catalog`);
  }

  getBranches(): Observable<ResponseAdminBranchCatalog> {
    return this._http.get<ResponseAdminBranchCatalog>(`${this.url}admin/branch/catalog`);
  }

  getBranchesById(companyId: string): Observable<ResponseAdminBranchCatalog> {
    return this._http.get<ResponseAdminBranchCatalog>(`${this.url}admin/branch/catalog/${companyId}`);
  }
}
