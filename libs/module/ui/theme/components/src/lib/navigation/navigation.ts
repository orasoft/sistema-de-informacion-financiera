import { FuseNavigation } from '@sif/module/ui/theme/fuse';

export const navigation: FuseNavigation[] = [
  {
    id: 'SIF',
    title: 'Sistema de informacion',
    type: 'group',
    children: [
      {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'item',
        icon: 'home',
        url: '/dashboard',
      }
    ]
  },
  {
    id: 'admin',
    title: 'Administracion',
    icon: 'settings_applications',
    type: 'collapsable',
    children: [
      // {
      //   id: 'admin_module',
      //   title: 'Modulos',
      //   type: 'item',
      //   icon: 'apps',
      //   url: '/admin/modules',
      // },
      {
        id: 'admin_companies',
        title: 'Empresas',
        type: 'item',
        icon: 'business',
        url: '/admin/companies',
      },
      {
        id: 'admin_branches',
        title: 'Sucursales',
        type: 'item',
        icon: 'storefront',
        url: '/admin/branches',
      },
      {
        id: 'admin_roles',
        title: 'Roles',
        type: 'item',
        icon: 'supervised_user_circle',
        url: '/admin/roles',
      },
      {
        id: 'admin_users',
        title: 'Usuarios',
        type: 'item',
        icon: 'people',
        url: '/admin/users',
      }
    ]
  }
];
