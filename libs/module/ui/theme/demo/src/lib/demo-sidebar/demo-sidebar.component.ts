import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'fuse-demo-sidebar',
  templateUrl: './demo-sidebar.component.html',
  styleUrls: ['./demo-sidebar.component.scss']
})
export class FuseDemoSidebarComponent {
  /**
   * Constructor
   */
  constructor() {
  }
}
