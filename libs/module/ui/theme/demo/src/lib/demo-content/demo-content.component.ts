import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'fuse-demo-content',
  templateUrl: './demo-content.component.html',
  styleUrls: ['./demo-content.component.scss']
})
export class FuseDemoContentComponent {
  /**
   * Constructor
   */
  constructor() {
  }
}
