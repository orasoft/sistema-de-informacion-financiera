import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NavbarVerticalStyle1Component } from './style-1.component';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import { FuseNavigationModule } from '@sif/module/ui/theme/components';


@NgModule({
  declarations: [
    NavbarVerticalStyle1Component
  ],
  imports: [
    MatButtonModule,
    MatIconModule,
    FuseSharedModule,
    FuseNavigationModule
  ],
  exports: [
    NavbarVerticalStyle1Component
  ]
})
export class NavbarVerticalStyle1Module {
}
