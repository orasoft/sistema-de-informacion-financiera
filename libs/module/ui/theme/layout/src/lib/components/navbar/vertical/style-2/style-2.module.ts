import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NavbarVerticalStyle2Component } from './style-2.component';
import { FuseNavigationModule } from '@sif/module/ui/theme/components';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';

@NgModule({
  declarations: [
    NavbarVerticalStyle2Component
  ],
  imports: [
    MatButtonModule,
    MatIconModule,
    FuseSharedModule,
    FuseNavigationModule
  ],
  exports: [
    NavbarVerticalStyle2Component
  ]
})
export class NavbarVerticalStyle2Module {
}
