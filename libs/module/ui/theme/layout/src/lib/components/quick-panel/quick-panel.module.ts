import { NgModule } from '@angular/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { QuickPanelComponent } from './quick-panel.component';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';

@NgModule({
  declarations: [
    QuickPanelComponent
  ],
  imports: [
    MatDividerModule,
    MatListModule,
    MatSlideToggleModule,
    FuseSharedModule
  ],
  exports: [
    QuickPanelComponent
  ]
})
export class QuickPanelModule {
}
