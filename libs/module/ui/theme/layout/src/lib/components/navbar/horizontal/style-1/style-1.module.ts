import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NavbarHorizontalStyle1Component } from './style-1.component';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import { FuseNavigationModule } from '@sif/module/ui/theme/components';


@NgModule({
  declarations: [
    NavbarHorizontalStyle1Component
  ],
  imports: [
    MatButtonModule,
    MatIconModule,
    FuseSharedModule,
    FuseNavigationModule
  ],
  exports: [
    NavbarHorizontalStyle1Component
  ]
})
export class NavbarHorizontalStyle1Module {
}
