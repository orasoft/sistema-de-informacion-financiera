import { Component } from '@angular/core';
import { environment } from '../../../../../../../../../apps/ng-sif/src/environments/environment';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  public version = environment.version;
  /**
   * Constructor
   */
  constructor() {
  }
}
