import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VerticalLayout3Component } from './layout-3.component';
import { ContentModule } from '../../components/content/content.module';
import { FooterModule } from '../../components/footer/footer.module';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { QuickPanelModule } from '../../components/quick-panel/quick-panel.module';
import { ToolbarModule } from '../../components/toolbar/toolbar.module';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import { FuseSidebarModule } from '@sif/module/ui/theme/components';


@NgModule({
  declarations: [
    VerticalLayout3Component
  ],
  imports: [
    RouterModule,
    FuseSharedModule,
    FuseSidebarModule,
    ContentModule,
    FooterModule,
    NavbarModule,
    QuickPanelModule,
    ToolbarModule
  ],
  exports: [
    VerticalLayout3Component
  ]
})
export class VerticalLayout3Module {
}
