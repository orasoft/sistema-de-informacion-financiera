# module-ui-theme-pipes

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test module-ui-theme-pipes` to execute the unit tests via [Jest](https://jestjs.io).
