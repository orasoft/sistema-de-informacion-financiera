import { NgModule } from '@angular/core';
import { DashboardComponent } from './components/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatIconModule,
  MatMenuModule,
  MatSelectModule,
  MatTabsModule
} from '@angular/material';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import { FuseWidgetModule } from '@sif/module/ui/theme/components';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartsModule } from 'ng2-charts';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatTabsModule,
    CommonModule,
    FuseSharedModule,
    ChartsModule,
    NgxChartsModule,
    FuseWidgetModule,
    FuseSharedModule
  ],
  declarations: [
    DashboardComponent
  ],
  providers: [],
  exports: []
})
export class DashboardModule {
}
