import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar } from '@angular/material';
import { AdminUser } from '@sif/shared/message/response';
import { AdminUserService } from '../../services/admin-user.service';
import { RequestAdminUser } from '@sif/shared/message/request';
import { AdminUsersShowPassTemporalComponent } from '../showPassTemporal/admin-users-show-pass-temporal.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'users-edit',
  templateUrl: './admin-users-edit.component.html',
  styleUrls: ['./admin-users-edit.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminUsersEditComponent implements OnInit {

  form: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Partial<AdminUser>,
              private readonly _adminUserService: AdminUserService,
              private _formBuilder: FormBuilder,
              private _dialogRef: MatDialogRef<AdminUsersEditComponent>,
              private _snackBar: MatSnackBar,
              private _router: Router,
              public dialog: MatDialog,
              ) {
  }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: [ this.data.name , Validators.required],
      username: [this.data.username || null ],
      avatar: [this.data.avatar || null ],
      email: [this.data.email || null ],
      status: [this.data.status || 'D']
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const request: Partial<RequestAdminUser> = {
        id: this.data.id,
        name: this.form.controls.name.value,
        username: this.form.controls.username.value,
        avatar: this.form.controls.avatar.value,
        email: this.form.controls.email.value,
        status: this.form.controls.status.value,
      };
      this._adminUserService.update(request).subscribe(data => {
        if (data.header.code === 200) {
          this._dialogRef.close();
        } else {
          this._snackBar.open(data.header.description);
        }
      }, error => {
        this._snackBar.open('NO FUE POSIBLE PROCESAR SU PETICION');
      });
    }
  }

  updatePass() {
    this._adminUserService.updatePass(this.data.id).subscribe( data => {
      if (data.header.code === 200) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.hasBackdrop = true;
        dialogConfig.data = {
          pass: data.data[0].password,
        };
        this.dialog.open(AdminUsersShowPassTemporalComponent, dialogConfig);
      }
    })
  }
}
