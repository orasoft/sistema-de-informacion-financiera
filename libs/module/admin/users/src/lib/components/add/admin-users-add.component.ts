import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseStatus } from '@sif/shared/types';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar } from '@angular/material';
import { AdminUserService } from '../../services/admin-user.service';
import { RequestAdminUser } from '@sif/shared/message/request';
import { AdminUsersShowPassTemporalComponent } from '../showPassTemporal/admin-users-show-pass-temporal.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'users-add',
  templateUrl: './admin-users-add.component.html',
  styleUrls: ['./admin-users-add.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminUsersAddComponent implements OnInit {

  form: FormGroup;

  constructor(private readonly _adminUserService: AdminUserService,
              private _formBuilder: FormBuilder,
              private _snackBar: MatSnackBar,
              private _router: Router,
              public dialog: MatDialog
              ) {
  }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: ['', Validators.required],
      username: [''],
      email: [''],
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const request: Partial<RequestAdminUser> = {
        name: this.form.controls.name.value,
        username: this.form.controls.username.value,
        email: this.form.controls.email.value,
        status: BaseStatus.active,
      };
      this._adminUserService.create(request).subscribe(data => {
        if (data.header.code === 200) {
          const dialogConfig = new MatDialogConfig();
          dialogConfig.disableClose = true;
          dialogConfig.autoFocus = true;
          dialogConfig.hasBackdrop = true;
          dialogConfig.data = {
            pass: data.data[0].password,
          };
          this.dialog.open(AdminUsersShowPassTemporalComponent, dialogConfig);
        } else {
          this._snackBar.open(data.header.description);
        }
      }, error => {
        this._snackBar.open('NO FUE POSIBLE PROCESAR SU PETICION');
      });
    }
  }

  return() {
    this._router.navigate(['admin/users']);
  }
}
