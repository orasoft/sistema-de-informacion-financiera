import { Component, HostListener, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { MatDialog, MatDialogConfig, MatPaginator, MatSnackBar, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { UsersDataSourceType } from '../../types/users-data-source.type';
import { AdminUserService } from '../../services/admin-user.service';
import { AdminUsersAddComponent } from '../add/admin-users-add.component';
import { AdminUsersEditComponent } from '../edit/admin-users-edit.component';
import { AdminUser } from '@sif/shared/message/response';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'users-list',
  templateUrl: './admin-users-list.component.html',
  styleUrls: ['./admin-users-list.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminUsersListComponent implements OnInit {

  public displayedColumns = [
    {def: 'name', mobile: false},
    {def: 'username', mobile: true},
    {def: 'email', mobile: false},
    {def: 'status', mobile: true}];

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  screenHeight: number;
  screenWidth: number;
  currentDisplay: string;

  public dataSource: UsersDataSourceType | null;

  constructor(private readonly adminUserService: AdminUserService,
              private _snackBar: MatSnackBar,
              private readonly _router: Router, public dialog: MatDialog) {
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    console.log(this.screenHeight, this.screenWidth);
    if ( this.screenWidth <= 840 ){
      this.currentDisplay = 'mobile';
    } else {
      this.currentDisplay = 'desktop';
    }
  }

  ngOnInit(): void {
    this.dataSource = new UsersDataSourceType(this.adminUserService, this.paginator, this.sort);
    this.getScreenSize()
  }

  getDisplayedColumns(): string[] {
    const isMobile = this.currentDisplay === 'mobile';
    return this.displayedColumns
      .filter(cd => !isMobile || cd.mobile)
      .map(cd => cd.def);
  }

  public newUser() {
    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.hasBackdrop = true;
    //
    // const dialogRef = this.dialog.open(AdminUsersAddComponent, dialogConfig);
    //
    // dialogRef.afterClosed().subscribe(result => {
    //   this.refresh();
    // });
    this._router.navigate(['admin/users_add'])
  }

  public showRecord(row: AdminUser) {
    this._router.navigate(['/admin/assign-role'], {queryParams: {idUser: row.id}});
    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.hasBackdrop = true;
    // dialogConfig.data = row;
    //
    // const dialogRefEdit = this.dialog.open(AdminUsersEditComponent, dialogConfig);
    //
    // dialogRefEdit.afterClosed().subscribe(result => {
    //   this.refresh();
    // });
  }

  public refresh() {
    this.getData().then( () => {
      this._snackBar.open('INFORMACION ACTUALIZADA.');
    }).catch(() => {
      this._snackBar.open('NO FUE POSIBLE ACTUALIZAR INFORMACION.');
    })
  }

  private getData() {
    return new Promise( (resolve, reject) => {
      this.adminUserService.getUsers().then( data => {
        this.dataSource.filterData(data);
        resolve();
      }).catch( e => {
        reject();
      });
    });
  }

  // public goToAssignRole() {
  //   this._router.navigate(['/admin/assign-role'])
  // }
}
