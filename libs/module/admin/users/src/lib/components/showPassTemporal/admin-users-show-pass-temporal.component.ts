import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'users-show-pass-temporal',
  templateUrl: './admin-users-show-pass-temporal.component.html',
  styleUrls: ['./admin-users-show-pass-temporal.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminUsersShowPassTemporalComponent {

  form: FormGroup;
  pass: string
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private _dialogRef: MatDialogRef<AdminUsersShowPassTemporalComponent>,
              private _snackBar: MatSnackBar,
              private _router: Router) {
    this.pass = data.pass;
  }
}
