import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import {
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatButtonModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';
import { AdminUsersEditComponent } from './components/edit/admin-users-edit.component';
import { AdminUsersListComponent } from './components/list/admin-users-list.component';
import { AdminUsersAddComponent } from './components/add/admin-users-add.component';
import { AdminUserService } from './services/admin-user.service';
import { AdminUsersShowPassTemporalComponent } from './components/showPassTemporal/admin-users-show-pass-temporal.component';


const routes: Routes = [
  {
    path: 'admin/users',
    component: AdminUsersListComponent,
    resolve: {
      AdminUserService
    }
  },
  {
    path: 'admin/users_add',
    component: AdminUsersAddComponent,
    resolve: {
      AdminUserService
    }
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatRadioModule,
    MatDialogModule
  ],
  declarations: [
    AdminUsersAddComponent,
    AdminUsersEditComponent,
    AdminUsersListComponent,
    AdminUsersShowPassTemporalComponent,
  ],
  providers: [
    AdminUserService,
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 3000, verticalPosition: 'top' } }
  ],
  entryComponents: [
    AdminUsersAddComponent,
    AdminUsersEditComponent,
    AdminUsersShowPassTemporalComponent,
  ],
  exports: []
})
export class AdminUsersModule {
}
