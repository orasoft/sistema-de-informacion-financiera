import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { RequestAdminUser } from '@sif/shared/message/request';
import { ResponseAdminUser } from '@sif/shared/message/response';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { environment } from '../../../../../../../apps/ng-sif/src/environments/environment';

@Injectable()
export class AdminUserService implements Resolve<any> {
  users: any[];
  onUsersChanged: BehaviorSubject<any>;
  private url = environment.apiUrl;
  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   * @param spinner
   */
  constructor(private _httpClient: HttpClient,
              private spinner: NgxSpinnerService) {
    this.onUsersChanged = new BehaviorSubject({});
    this.users = [];
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {

      Promise.all([
        this.getUsers()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getUsers(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.get(`${this.url}admin/user`)
        .subscribe((response: any) => {
          this.users = response.data;
          this.onUsersChanged.next(this.users);
          resolve(response);
        }, reject);
    });
  }

  create(requestAdminUser: Partial<RequestAdminUser>): Observable<ResponseAdminUser> {
    this.spinner.show();
    return this._httpClient.post<ResponseAdminUser>(`${this.url}admin/user`, requestAdminUser).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }

  update(requestAdminUser: Partial<RequestAdminUser>): Observable<ResponseAdminUser> {
    this.spinner.show();
    return this._httpClient.put<ResponseAdminUser>(`${this.url}admin/user/${requestAdminUser.id}`, requestAdminUser).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }

  updatePass(id): Observable<ResponseAdminUser> {
    this.spinner.show();
    return this._httpClient.put<ResponseAdminUser>(`${this.url}admin/user/resetpass/${id}`, {} ).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }

  public hideSpinner() {
    setTimeout(() =>
        this.spinner.hide(),
      1000);
  }
}
