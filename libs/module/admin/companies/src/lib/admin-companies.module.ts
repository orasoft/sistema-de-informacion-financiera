import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminCompaniesListComponent } from './components/list/admin-companies-list.component';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import {
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatButtonModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';
import { AdminCompanyService } from './services/admin-company.service';
import { AdminCompaniesAddComponent } from './components/add/admin-companies-add.component';
import { AdminCompaniesEditComponent } from './components/edit/admin-companies-edit.component';

const routes: Routes = [
  {
    path: 'admin/companies',
    component: AdminCompaniesListComponent,
    resolve: {
      AdminCompanyService
    }
  },
  {
    path: 'admin/companies_add',
    component: AdminCompaniesAddComponent,
    resolve: {
      AdminCompanyService
    }
  },
  {
    path: 'admin/companies_edit',
    component: AdminCompaniesEditComponent,
    resolve: {
      AdminCompanyService
    }
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatRadioModule,
    MatDialogModule
  ],
  declarations: [
    AdminCompaniesListComponent,
    AdminCompaniesAddComponent,
    AdminCompaniesEditComponent
  ],
  providers: [
    AdminCompanyService,
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 3000, verticalPosition: 'top' } }
  ],
  entryComponents: [
    AdminCompaniesAddComponent,
    AdminCompaniesEditComponent
  ],
  exports: []
})
export class AdminCompaniesModule {
}
