import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { RequestAdminCompany } from '@sif/shared/message/request';
import { ResponseAdminCompany } from '@sif/shared/message/response';
import { environment } from '../../../../../../../apps/ng-sif/src/environments/environment';

@Injectable()
export class AdminCompanyService implements Resolve<any> {
  companies: any[];
  onCompaniesChanged: BehaviorSubject<any>;
  private url = environment.apiUrl;
  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {
    this.onCompaniesChanged = new BehaviorSubject({});
    this.companies = [];
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {

      Promise.all([
        this.getCompanies()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getCompanies(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.get(`${this.url}admin/company`)
        .subscribe((response: any) => {
          this.companies = response.data;
          this.onCompaniesChanged.next(this.companies);
          resolve(response);
        }, reject);
    });
  }

  create(requestAdminCompany: Partial<RequestAdminCompany>): Observable<ResponseAdminCompany> {
    return this._httpClient.post<ResponseAdminCompany>(`${this.url}admin/company`, requestAdminCompany);
  }

  update(requestAdminCompany: Partial<RequestAdminCompany>): Observable<ResponseAdminCompany> {
    return this._httpClient.put<ResponseAdminCompany>(`${this.url}admin/company/${requestAdminCompany.id}`, requestAdminCompany);
  }
}
