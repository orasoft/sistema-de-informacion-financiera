import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { AdminCompanyService } from '../../services/admin-company.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RequestAdminCompany } from '@sif/shared/message/request';
import { BaseStatus } from '@sif/shared/types';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'companies-add',
  templateUrl: './admin-companies-add.component.html',
  styleUrls: ['./admin-companies-add.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminCompaniesAddComponent implements OnInit {

  form: FormGroup;

  constructor(private readonly _adminCompanyService: AdminCompanyService,
              private _formBuilder: FormBuilder,
              private _snackBar: MatSnackBar,
              private _router: Router) {
  }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: ['', Validators.required],
      nit: [''],
      dui: [''],
      nrc: [''],
      address: [''],
      type: ['', Validators.required],
      phone: ['']
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const request: Partial<RequestAdminCompany> = {
        name: this.form.controls.name.value,
        phone: this.form.controls.phone.value,
        nit: this.form.controls.nit.value,
        dui: this.form.controls.dui.value,
        nrc: this.form.controls.nrc.value,
        type: this.form.controls.type.value,
        status: BaseStatus.active,
        address: this.form.controls.address.value
      };
      this._adminCompanyService.create(request).subscribe(data => {
        if (data.header.code === 200) {
          this._router.navigate(['admin/companies']);
        } else {
          this._snackBar.open(data.header.description);
        }
      }, error => {
        this._snackBar.open('NO FUE POSIBLE PROCESAR SU PETICION');
      });
    }
  }

  return() {
    this._router.navigate(['admin/companies']);
  }
}
