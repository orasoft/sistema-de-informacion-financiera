import { Component, HostListener, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { MatDialog, MatDialogConfig, MatPaginator, MatSnackBar, MatSort } from '@angular/material';
import { CompaniesDataSourceType } from '../../types/companies-data-source.type';
import { AdminCompanyService } from '../../services/admin-company.service';
import { Router } from '@angular/router';
import { AdminCompaniesAddComponent } from '../add/admin-companies-add.component';
import { AdminCompany } from '@sif/shared/message/response';
import { AdminCompaniesEditComponent } from '../edit/admin-companies-edit.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'companies-list',
  templateUrl: './admin-companies-list.component.html',
  styleUrls: ['./admin-companies-list.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminCompaniesListComponent implements OnInit {

  public displayedColumns = [
    {def: 'name', mobile: true},
    {def: 'nit', mobile: false},
    {def: 'phone', mobile: false},
    {def: 'status', mobile: true},
  ];

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  screenHeight: number;
  screenWidth: number;
  currentDisplay: string;

  public dataSource: CompaniesDataSourceType | null;

  constructor(private readonly adminCompanyService: AdminCompanyService,
              private _snackBar: MatSnackBar,
              private readonly _router: Router, public dialog: MatDialog) {

  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    console.log(this.screenHeight, this.screenWidth);
    if ( this.screenWidth <= 840 ){
      this.currentDisplay = 'mobile';
    } else {
      this.currentDisplay = 'desktop';
    }
  }

  ngOnInit(): void {
    this.dataSource = new CompaniesDataSourceType(this.adminCompanyService, this.paginator, this.sort);
    this.getScreenSize();
  }

  getDisplayedColumns(): string[] {
    const isMobile = this.currentDisplay === 'mobile';
    return this.displayedColumns
      .filter(cd => !isMobile || cd.mobile)
      .map(cd => cd.def);
  }

  public newCompany() {
    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.hasBackdrop = true;
    //
    // const dialogRef = this.dialog.open(AdminCompaniesAddComponent, dialogConfig);
    //
    // dialogRef.afterClosed().subscribe(result => {
    //   this.refresh();
    // });
    this._router.navigate(['admin/companies_add']);
  }

  public showRecord(row: AdminCompany) {
    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.hasBackdrop = true;
    // dialogConfig.data = row;
    //
    // const dialogRefEdit = this.dialog.open(AdminCompaniesEditComponent, dialogConfig);
    //
    // dialogRefEdit.afterClosed().subscribe(result => {
    //   this.refresh();
    // });
    this._router.navigate(['admin/companies_edit'], {queryParams: {row: JSON.stringify(row)}});
  }

  public refresh() {
    this.getData().then( () => {
      this._snackBar.open('INFORMACION ACTUALIZADA.');
    }).catch(() => {
      this._snackBar.open('NO FUE POSIBLE ACTUALIZAR INFORMACION.');
    })
  }

  private getData() {
    return new Promise( (resolve, reject) => {
      this.adminCompanyService.getCompanies().then( data => {
        this.dataSource.filterData(data);
        resolve();
      }).catch( e => {
        reject();
      });
    });
  }
}
