import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { AdminCompanyService } from '../../services/admin-company.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RequestAdminCompany } from '@sif/shared/message/request';
import { BaseStatus } from '@sif/shared/types';
import { ActivatedRoute, Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { AdminCompany } from '@sif/shared/message/response';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'companies-add',
  templateUrl: './admin-companies-edit.component.html',
  styleUrls: ['./admin-companies-edit.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminCompaniesEditComponent implements OnInit {

  form: FormGroup;
  data: AdminCompany;

  constructor(
              private readonly _adminCompanyService: AdminCompanyService,
              private _formBuilder: FormBuilder,
              private _snackBar: MatSnackBar,
              private _router: Router,
              private _activatedRoute: ActivatedRoute) {
    this.data = JSON.parse(this._activatedRoute.snapshot.queryParams.row);
  }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: [ this.data.name , Validators.required],
      nit: [this.data.nit || null ],
      dui: [this.data.dui || null ],
      nrc: [this.data.nrc || null ],
      address: [this.data.address || null],
      type: [this.data.type, Validators.required],
      phone: [this.data.phone || null],
      status: [this.data.status || 'D']
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const request: Partial<RequestAdminCompany> = {
        id: this.data.id,
        name: this.form.controls.name.value,
        phone: this.form.controls.phone.value,
        nit: this.form.controls.nit.value,
        dui: this.form.controls.dui.value,
        nrc: this.form.controls.nrc.value,
        type: this.form.controls.type.value,
        status: this.form.controls.status.value,
        address: this.form.controls.address.value
      };
      this._adminCompanyService.update(request).subscribe(data => {
        if (data.header.code === 200) {
          this._router.navigate(['admin/companies']);
        } else {
          this._snackBar.open(data.header.description);
        }
      }, error => {
        this._snackBar.open('NO FUE POSIBLE PROCESAR SU PETICION');
      });
    }
  }

  return() {
    this._router.navigate(['admin/companies']);
  }
}
