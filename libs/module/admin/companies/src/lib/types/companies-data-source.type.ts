import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { AdminCompanyService } from '../services/admin-company.service';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { FuseUtils } from '@sif/module/ui/theme/fuse';

export class CompaniesDataSourceType extends DataSource<any> {

  private _filterChange = new BehaviorSubject('');
  private _filteredDataChange = new BehaviorSubject('');

  constructor(private readonly adminCompanyService: AdminCompanyService,
              private _matPaginator: MatPaginator,
              private _matSort: MatSort) {
    super();
    this.filteredData = this.adminCompanyService.companies;
  }

  get filteredData(): any {
    return this._filteredDataChange.value;
  }

  set filteredData(value: any) {
    this._filteredDataChange.next(value);
  }

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  connect(collectionViewer: CollectionViewer): Observable<any[] | ReadonlyArray<any>> {
    const displayDataChanges = [
      this.adminCompanyService.onCompaniesChanged,
      this._matPaginator.page,
      this._filterChange
    ];
    return merge(...displayDataChanges)
      .pipe(
        map(() => {
            let data = this.adminCompanyService.companies.slice();
            data = this.filterData(data);
            this.filteredData = [...data];
            const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
            return data.splice(startIndex, this._matPaginator.pageSize);
          }
        ));
  }

  disconnect(collectionViewer: CollectionViewer): void {
  }

  filterData(data): any {
    if (!this.filter) {
      return data;
    }
    return FuseUtils.filterArrayByString(data, this.filter);
  }

}
