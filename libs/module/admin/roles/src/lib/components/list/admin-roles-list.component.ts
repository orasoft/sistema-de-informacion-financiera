import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { MatDialog, MatDialogConfig, MatPaginator, MatSnackBar, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { RolesDataSourceType } from '../../types/roles-data-source.type';
import { AdminRoleService } from '../../services/admin-role.service';
import { AdminRolesAddComponent } from '../add/admin-roles-add.component';
import { AdminRole } from '@sif/shared/message/response';
import { AdminRolesEditComponent } from '../edit/admin-roles-edit.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'roles-list',
  templateUrl: './admin-roles-list.component.html',
  styleUrls: ['./admin-roles-list.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminRolesListComponent implements OnInit {

  public displayedColumns: string[] = ['name', 'status'];

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  public dataSource: RolesDataSourceType | null;

  constructor(private readonly adminRoleService: AdminRoleService,
              private _snackBar: MatSnackBar,
              private readonly _router: Router, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.dataSource = new RolesDataSourceType(this.adminRoleService, this.paginator, this.sort);
  }

  public newUser() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.hasBackdrop = true;

    const dialogRef = this.dialog.open(AdminRolesAddComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    });
  }

  public showRecord(row: AdminRole) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.data = row;

    const dialogRefEdit = this.dialog.open(AdminRolesEditComponent, dialogConfig);

    dialogRefEdit.afterClosed().subscribe(result => {
      this.refresh();
    });
  }

  public refresh() {
    this.getData().then( () => {
      this._snackBar.open('INFORMACION ACTUALIZADA.');
    }).catch(() => {
      this._snackBar.open('NO FUE POSIBLE ACTUALIZAR INFORMACION.');
    })
  }

  private getData() {
    return new Promise( (resolve, reject) => {
      this.adminRoleService.getRoles().then( data => {
        this.dataSource.filterData(data);
        resolve();
      }).catch( e => {
        reject();
      });
    });
  }
}
