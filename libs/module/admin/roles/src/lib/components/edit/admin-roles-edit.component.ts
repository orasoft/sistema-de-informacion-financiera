import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { AdminRole } from '@sif/shared/message/response';
import { AdminRoleService } from '../../services/admin-role.service';
import { RequestAdminRole } from '@sif/shared/message/request';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'roles-add',
  templateUrl: './admin-roles-edit.component.html',
  styleUrls: ['./admin-roles-edit.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminRolesEditComponent implements OnInit {

  form: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Partial<AdminRole>,
              private readonly _adminRoleService: AdminRoleService,
              private _formBuilder: FormBuilder,
              private _dialogRef: MatDialogRef<AdminRolesEditComponent>,
              private _snackBar: MatSnackBar,
              private _router: Router) {
  }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: [ this.data.name , Validators.required],
      status: [this.data.status || 'D']
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const request: Partial<RequestAdminRole> = {
        id: this.data.id,
        name: this.form.controls.name.value,
        status: this.form.controls.status.value,
      };
      this._adminRoleService.update(request).subscribe(data => {
        if (data.header.code === 200) {
          this._dialogRef.close();
        } else {
          this._snackBar.open(data.header.description);
        }
      }, error => {
        this._snackBar.open('NO FUE POSIBLE PROCESAR SU PETICION');
      });
    }
  }
}
