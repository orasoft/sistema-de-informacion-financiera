import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseStatus } from '@sif/shared/types';
import { Router } from '@angular/router';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { AdminRoleService } from '../../services/admin-role.service';
import { RequestAdminRole } from '@sif/shared/message/request';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'roles-add',
  templateUrl: './admin-roles-add.component.html',
  styleUrls: ['./admin-roles-add.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminRolesAddComponent implements OnInit {

  form: FormGroup;

  constructor(private readonly _adminRoleService: AdminRoleService,
              private _formBuilder: FormBuilder,
              private _dialogRef: MatDialogRef<AdminRolesAddComponent>,
              private _snackBar: MatSnackBar,
              private _router: Router) {
  }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const request: Partial<RequestAdminRole> = {
        name: this.form.controls.name.value,
        status: BaseStatus.active,
      };
      this._adminRoleService.create(request).subscribe(data => {
        if (data.header.code === 200) {
          this._dialogRef.close();
        } else {
          this._snackBar.open(data.header.description);
        }
      }, error => {
        this._snackBar.open('NO FUE POSIBLE PROCESAR SU PETICION');
      });
    }
  }
}
