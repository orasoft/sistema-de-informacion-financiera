import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import {
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatButtonModule,
  MatChipsModule, MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule, MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule, MatTabsModule
} from '@angular/material';
import { AdminRolesAddComponent } from './components/add/admin-roles-add.component';
import { AdminRolesEditComponent } from './components/edit/admin-roles-edit.component';
import { AdminRolesListComponent } from './components/list/admin-roles-list.component';
import { AdminRoleService } from './services/admin-role.service';


const routes: Routes = [
  {
    path: 'admin/roles',
    component: AdminRolesListComponent,
    resolve: {
      AdminRoleService
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatRadioModule,
    MatDialogModule
  ],
  declarations: [
    AdminRolesAddComponent,
    AdminRolesEditComponent,
    AdminRolesListComponent
  ],
  providers: [
    AdminRoleService,
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 3000, verticalPosition: 'top' } }
  ],
  entryComponents: [
    AdminRolesAddComponent,
    AdminRolesEditComponent,
  ],
  exports: []
})
export class AdminRolesModule{
}
