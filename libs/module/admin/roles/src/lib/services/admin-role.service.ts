import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ResponseAdminRole } from '@sif/shared/message/response';
import { RequestAdminRole } from '@sif/shared/message/request';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../../../apps/ng-sif/src/environments/environment';

@Injectable()
export class AdminRoleService implements Resolve<any> {

  roles: any[];
  onRolesChanged: BehaviorSubject<any>;
  private url = environment.apiUrl;
  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {
    this.onRolesChanged = new BehaviorSubject({});
    this.roles = [];
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {

      Promise.all([
        this.getRoles()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getRoles(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.get(`${this.url}admin/role`)
        .subscribe((response: any) => {
          this.roles = response.data;
          this.onRolesChanged.next(this.roles);
          resolve(response);
        }, reject);
    });
  }

  create(requestAdminRole: Partial<RequestAdminRole>): Observable<ResponseAdminRole> {
    return this._httpClient.post<ResponseAdminRole>(`${this.url}admin/role`, requestAdminRole);
  }

  update(requestAdminRole: Partial<RequestAdminRole>): Observable<ResponseAdminRole> {
    return this._httpClient.put<ResponseAdminRole>(`${this.url}admin/role/${requestAdminRole.id}`, requestAdminRole);
  }

}
