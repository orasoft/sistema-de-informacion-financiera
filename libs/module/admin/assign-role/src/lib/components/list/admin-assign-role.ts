import { AfterViewInit, Component, HostListener, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { AdminAssignRoleService } from '../../services/admin-assign-role.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminUser, AdminUserRoleBranch } from '@sif/shared/message/response';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RequestAdminUser } from '@sif/shared/message/request';
// tslint:disable-next-line:nx-enforce-module-boundaries
import { AdminUserService } from '../../../../../users/src/lib/services/admin-user.service';
import { MatDialog, MatDialogConfig, MatSelect, MatSnackBar } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { take, takeUntil } from 'rxjs/operators';
// tslint:disable-next-line:nx-enforce-module-boundaries
import { AdminUsersShowPassTemporalComponent } from '../../../../../users/src/lib/components/showPassTemporal/admin-users-show-pass-temporal.component';

@Component({
  selector: 'app-assign-role',
  templateUrl: './admin-assign-role.html',
  styleUrls: ['./admin-assign-role.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminAssignRoleComponent implements OnInit, OnDestroy, AfterViewInit {

  private idUserSelected;
  public infoUser: Partial<AdminUser>;
  public form: FormGroup;
  public infoUserName: string;

  protected roleListAll = [];
  protected roleList = [];
  public bankCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public bankFilterCtrl: FormControl = new FormControl();

  /** list of banks filtered by search keyword */
  public filteredBanks: ReplaySubject<any> = new ReplaySubject<any>(1);
  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;

  protected _onDestroy = new Subject<void>();

  screenHeight: number;
  screenWidth: number;
  currentDisplay: string;

  displayedColumns: string[] = ['role', 'actions'];
  dataSource = [];
  data = [];
  public companyIdSelected: string;
  public branchIdSelected: string;
  public roleIdSelected: string;
  constructor(private _adminAssignRoleService: AdminAssignRoleService,
              private _adminUserService: AdminUserService,
              private _router: Router,
              private _snackBar: MatSnackBar,
              private _formBuilder: FormBuilder,
              private activeRouter: ActivatedRoute,
              private toastr: ToastrService,
              public dialog: MatDialog,
              ) {
    this.idUserSelected = this.activeRouter.snapshot.queryParams.idUser;
    this.getInfoUser(this.idUserSelected);
  }

  ngOnInit(): void {
    this.getCatalogRole();
    this.getScreenSize();
    this.form = this._formBuilder.group({
      name: [ '', Validators.required],
      username: [ '', Validators.required],
      email: [ '', Validators.required],
      status: [ '', Validators.required],
    });
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    console.log(this.screenHeight, this.screenWidth);
    if ( this.screenWidth <= 767 ){
      this.currentDisplay = 'mobile';
    } else {
      this.currentDisplay = 'desktop';
    }
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredBanks
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelect.compareWith = (a, b) => a && b && a.id === b.id;
      });
  }

  onCompanySelected(companyId: string) {
    this.companyIdSelected = companyId;
    this.branchIdSelected = undefined;
    this.data = [];
    this.roleList = [];
    this.bankCtrl.setValue(this.roleList[0]);
    this.filteredBanks.next(this.roleList.slice());
    this.bankFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBanks();
      });

    this.dataSource = [...this.data];
  }

  onBranchSelected(branchId: string) {
    this.branchIdSelected = branchId;
    this.getByUserAndBranch();
  }

  getByUserAndBranch() {
    this._adminAssignRoleService.getByUserAndBranch(this.idUserSelected, this.branchIdSelected).subscribe( respUB => {
      this.data = [];
      this.roleList = [];
      this.roleListAll.forEach( role => {
        let exist = false;
        respUB.data.forEach( line => {
          if (line.role.id === role.id) {
            exist = true;
            this.data.push({
              id: line.role.id,
              role: line.role.name,
              idAsc: line.id,
            });
          }
        });
        if (!exist) {
          this.roleList.push({
            id: role.id,
            name: role.name,
          })
        }
      });

      this.bankCtrl.setValue(this.roleList[0]);
      this.filteredBanks.next(this.roleList.slice());
      this.bankFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterBanks();
        });

      this.dataSource = [...this.data];
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const request: Partial<RequestAdminUser> = {
        id: this.idUserSelected,
        name: this.form.getRawValue().name,
        username: this.form.getRawValue().username,
        email: this.form.getRawValue().email,
        status: this.form.getRawValue().status,
      };
      this._adminUserService.update(request).subscribe(data => {
        if (data.header.code === 200) {
          this.toastr.success('Información actualizada con éxito');
        } else {
          this.toastr.error('No fue posible actualizar la información');
        }
      }, error => {
        this.toastr.error('Error Inesperado, contacte al administrador');
      });
    } else {
      this.toastr.warning('Complete la información requerida');
    }
  }

  public getCatalogRole() {
    this._adminAssignRoleService.getCatalog().subscribe( resp => {
      if(resp.header.code === 200) {
        this.roleListAll =  resp.catalog;
        this.roleList = resp.catalog;
      }
    });
  }

  protected filterBanks() {
    if (!this.roleList) {
      return;
    }
    // get the search keyword
    let search = this.bankFilterCtrl.value;
    if (!search) {
      this.filteredBanks.next(this.roleList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredBanks.next(
      this.roleList.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }

  public roleSelected() {
    this.roleIdSelected = this.bankCtrl.value.id;

    if(this.roleIdSelected !== null &&
      this.roleIdSelected !== undefined &&
      this.companyIdSelected !== null &&
      this.companyIdSelected !== undefined &&
      this.branchIdSelected !== null &&
      this.branchIdSelected !== undefined) {


      const params = {
        userId: this.idUserSelected,
        roleId: this.roleIdSelected,
        branchId: this.branchIdSelected
      }

      this._adminAssignRoleService.createUserRoleBranch(params).subscribe( resp => {
        console.log(`resp ====> ${resp}`);
        if (resp.header.code === 200) {
          this.getByUserAndBranch();
        }
      })
    } else {
      this.toastr.warning('Seleccione la información requerida');
    }
  }

  public deleteRole(data) {

    this._adminAssignRoleService.deleteById(data.idAsc).subscribe( resp => {
      if (resp.header.code === 200) {
        this.getByUserAndBranch();
        this.toastr.success('Relación eliminada con éxito');
      } else {
        this.toastr.warning('No se pudo eliminar la relación');
      }
    });
  }

  public getInfoUser(id) {
    this._adminAssignRoleService.getInfoUser(id).subscribe( resp => {
      if (resp.header.code === 200) {
        this.infoUser = resp.data[0];
        this.infoUserName = this.infoUser.name;
        this.form.patchValue({
          name: this.infoUser.name,
          username: this.infoUser.username,
          email: this.infoUser.email,
          status: this.infoUser.status,
        })
      }
    });
  }

  public return() {
    this._router.navigate(['admin/users']);
  }

  public updatePass() {
    this._adminUserService.updatePass(this.idUserSelected).subscribe( data => {
      if (data.header.code === 200) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.hasBackdrop = true;
        dialogConfig.data = {
          pass: data.data[0].password,
        };
        this.dialog.open(AdminUsersShowPassTemporalComponent, dialogConfig);
      }
    })
  }

}
