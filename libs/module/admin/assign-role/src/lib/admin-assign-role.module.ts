import { ChangeDetectorRef, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import {
  MatButtonModule,
  MatChipsModule, MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule, MatRadioModule,
  MatRippleModule, MatSelect,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule, MatTabsModule
} from '@angular/material';
import { AdminAssignRoleComponent } from './components/list/admin-assign-role';
import { UIAppSharedModule } from '@sif/module/ui/app/shared';
import { AdminAssignRoleService } from './services/admin-assign-role.service';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

const routes: Routes = [
  {
    path: 'admin/assign-role',
    component: AdminAssignRoleComponent,
    // resolve: {
    //   AdminUserService
    // }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatRadioModule,
    MatDialogModule,
    UIAppSharedModule,
    MatGridListModule,
    NgxMatSelectSearchModule
  ],
  declarations: [
    AdminAssignRoleComponent,
  ],
  providers: [
    AdminAssignRoleService,
  ],
  entryComponents: [],
  exports: []
})
export class AdminAssignRoleModule {

}
