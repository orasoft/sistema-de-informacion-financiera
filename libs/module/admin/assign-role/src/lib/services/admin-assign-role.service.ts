import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseAdminUser, ResponseAdminUserRoleBranch } from '@sif/shared/message/response';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { environment } from '../../../../../../../apps/ng-sif/src/environments/environment';


@Injectable()
export class AdminAssignRoleService {

  private url = environment.apiUrl;

  constructor(private _httpClient: HttpClient,
              private spinner: NgxSpinnerService) {}

  public hideSpinner() {
    setTimeout(() =>
        this.spinner.hide(),
      1000);
  }

  public getCatalog(): Observable<any> {
    this.spinner.show();
    return this._httpClient.get<any>(`${this.url}admin/role/catalog`).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }

  public getInfoUser(id: string): Observable<ResponseAdminUser> {
    this.spinner.show();
    return this._httpClient.get<ResponseAdminUser>(`${this.url}admin/user/${id}`).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }

  public createUserRoleBranch(body): Observable<ResponseAdminUserRoleBranch> {
    this.spinner.show();
    return this._httpClient.post<ResponseAdminUserRoleBranch>(`${this.url}admin/user_role_branch`, body).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }

  public getByUserAndBranch(userId: string, branchId: string): Observable<ResponseAdminUserRoleBranch> {
    this.spinner.show();
    return this._httpClient.get<ResponseAdminUserRoleBranch>(`${this.url}admin/user_role_branch/user/${userId}/${branchId}`).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }

  public deleteById(id: string): Observable<ResponseAdminUserRoleBranch> {
    this.spinner.show();
    return this._httpClient.delete<ResponseAdminUserRoleBranch>(`${this.url}admin/user_role_branch/${id}`).pipe(finalize(() => {
      this.hideSpinner();
    }));
  }
}
