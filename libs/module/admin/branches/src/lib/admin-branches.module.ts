import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FuseSharedModule } from '@sif/module/ui/theme/shared';
import {
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatButtonModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule
} from '@angular/material';
import { AdminBranchService } from './services/admin-branch.service';
import { AdminBranchesListComponent } from './components/list/admin-branches-list.component';
import { AdminBranchesEditComponent } from './components/edit/admin-branches-edit.component';
import { AdminBranchesAddComponent } from './components/add/admin-branches-add.component';
import { UIAppSharedModule } from '@sif/module/ui/app/shared';


const routes: Routes = [
  {
    path: 'admin/branches',
    component: AdminBranchesListComponent,
    resolve: {
      AdminBranchService
    }
  },
  {
    path: 'admin/branches_add',
    component: AdminBranchesAddComponent,
    resolve: {
      AdminBranchService
    }
  },
  {
    path: 'admin/branches_edit',
    component: AdminBranchesEditComponent,
    resolve: {
      AdminBranchService
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatRadioModule,
    MatDialogModule,
    UIAppSharedModule
  ],
  declarations: [
    AdminBranchesListComponent,
    AdminBranchesAddComponent,
    AdminBranchesEditComponent
  ],
  providers: [
    AdminBranchService,
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 3000, verticalPosition: 'top' } }
  ],
  entryComponents: [
    AdminBranchesAddComponent,
    AdminBranchesEditComponent
  ]
})
export class AdminBranchesModule {
}
