import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { RequestAdminBranch } from '@sif/shared/message/request';
import { AdminBranch, ResponseAdminBranch, ResponseAdminCompany } from '@sif/shared/message/response';
import { environment } from '../../../../../../../apps/ng-sif/src/environments/environment';

@Injectable()
export class AdminBranchService implements Resolve<any> {
  branches: any[];
  onBranchesChanged: BehaviorSubject<any>;
  private url = environment.apiUrl;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {
    this.onBranchesChanged = new BehaviorSubject({});
    this.branches = [];
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {

      Promise.all([
        this.getBranchesWithCompany()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getBranchesWithCompany(): Promise<AdminBranch[]> {
    return new Promise((resolve, reject) => {
      this._getCompany().then(companyId => {
        this._httpClient.get(`${this.url}admin/branch/with-company`).subscribe((response: any) => {
          this.branches = response.data;
          this.onBranchesChanged.next(this.branches);
          resolve(response);
        }, reject);
      }).catch(err => {
        reject(err);
      });
    });
  }

  create(requestAdminBranch: Partial<RequestAdminBranch>): Observable<ResponseAdminBranch> {
    return this._httpClient.post<ResponseAdminCompany>(`${this.url}admin/branch/${requestAdminBranch.companyId}`, requestAdminBranch);
  }

  update(requestAdminBranch: Partial<RequestAdminBranch>): Observable<ResponseAdminBranch> {
    return this._httpClient.put<ResponseAdminCompany>(`${this.url}admin/branch/${requestAdminBranch.id}`, requestAdminBranch);
  }

  private _getCompany(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this._httpClient.get<ResponseAdminCompany>(`${this.url}admin/company`).subscribe((response: ResponseAdminCompany) => {
        if (response.data.length > 0) {
          resolve(response.data[0].id);
        } else {
          reject();
        }
      });
    });
  }
}
