import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { FuseUtils } from '@sif/module/ui/theme/fuse';
import { AdminBranchService } from '../services/admin-branch.service';
import { AdminBranch } from '@sif/shared/message/response';

export class BranchesDataSourceType extends DataSource<AdminBranch[]> {

  private _filterChange = new BehaviorSubject('');
  private _filteredDataChange = new BehaviorSubject<AdminBranch[]>([]);

  constructor(private readonly adminBranchService: AdminBranchService,
              private _matPaginator: MatPaginator,
              private _matSort: MatSort) {
    super();
    this.filteredData = this.adminBranchService.branches;
  }

  get filteredData(): AdminBranch[] {
    return this._filteredDataChange.value;
  }

  set filteredData(value: AdminBranch[]) {
    this._filteredDataChange.next(value);
  }

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  connect(collectionViewer: CollectionViewer): Observable<any[] | ReadonlyArray<any>> {
    const displayDataChanges = [
      this.adminBranchService.onBranchesChanged,
      this._matPaginator.page,
      this._filterChange
    ];
    return merge(...displayDataChanges)
      .pipe(
        map(() => {
            let data = this.adminBranchService.branches.slice();
            data = this.filterData(data);
            this.filteredData = [...data];
            const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
            return data.splice(startIndex, this._matPaginator.pageSize);
          }
        ));
  }

  disconnect(collectionViewer: CollectionViewer): void {
  }

  filterData(data): AdminBranch[] {
    if (!this.filter) {
      return data;
    }
    return FuseUtils.filterArrayByString(data, this.filter);
  }

}
