import { Component, HostListener, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { MatDialog, MatDialogConfig, MatPaginator, MatSnackBar, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { BranchesDataSourceType } from '../../types/branches-data-source.type';
import { AdminBranchService } from '../../services/admin-branch.service';
import { AdminBranch } from '@sif/shared/message/response';
import { AdminBranchesAddComponent } from '../add/admin-branches-add.component';
import { AdminBranchesEditComponent } from '../edit/admin-branches-edit.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'companies-list',
  templateUrl: './admin-branches-list.component.html',
  styleUrls: ['./admin-branches-list.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminBranchesListComponent implements OnInit {

  public displayedColumns = [
    {def: 'company', mobile: false},
    {def: 'name', mobile: true},
    {def: 'phone', mobile: false},
    {def: 'status', mobile: true}
  ];

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  screenHeight: number;
  screenWidth: number;
  currentDisplay: string;

  public dataSource: BranchesDataSourceType | null;

  constructor(private readonly adminCompanyService: AdminBranchService,
              private _snackBar: MatSnackBar,
              private readonly _router: Router, public dialog: MatDialog,
              ) {
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    console.log(this.screenHeight, this.screenWidth);
    if ( this.screenWidth <= 840 ){
      this.currentDisplay = 'mobile';
    } else {
      this.currentDisplay = 'desktop';
    }
  }

  ngOnInit(): void {
    this.dataSource = new BranchesDataSourceType(this.adminCompanyService, this.paginator, this.sort);
    this.getScreenSize();
  }

  getDisplayedColumns(): string[] {
    const isMobile = this.currentDisplay === 'mobile';
    return this.displayedColumns
      .filter(cd => !isMobile || cd.mobile)
      .map(cd => cd.def);
  }

  public newCompany() {
    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.hasBackdrop = true;
    //
    // const dialogRef = this.dialog.open(AdminBranchesAddComponent, dialogConfig);
    //
    // dialogRef.afterClosed().subscribe(result => {
    //   this.refresh();
    // });
    this._router.navigate(['admin/branches_add']);
  }

  public showRecord(row: AdminBranch) {
    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.hasBackdrop = true;
    // dialogConfig.data = row;
    //
    // const dialogRefEdit = this.dialog.open(AdminBranchesEditComponent, dialogConfig);
    //
    // dialogRefEdit.afterClosed().subscribe(result => {
    //   this.refresh();
    // });
    this._router.navigate(['admin/branches_edit'], {queryParams: {row: JSON.stringify(row)}});
  }

  public refresh() {
    this.getData().then( () => {
      this._snackBar.open('INFORMACION ACTUALIZADA.');
    }).catch(() => {
      this._snackBar.open('NO FUE POSIBLE ACTUALIZAR INFORMACION.');
    })
  }

  private getData() {
    return new Promise( (resolve, reject) => {
      this.adminCompanyService.getBranchesWithCompany().then(data => {
        this.dataSource.filterData(data);
        resolve();
      }).catch( e => {
        reject();
      });
    });
  }
}
