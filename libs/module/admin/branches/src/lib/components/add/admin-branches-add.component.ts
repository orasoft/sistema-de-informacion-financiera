import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RequestAdminBranch } from '@sif/shared/message/request';
import { BaseStatus } from '@sif/shared/types';
import { Router } from '@angular/router';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { AdminBranchService } from '../../services/admin-branch.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'branches-add',
  templateUrl: './admin-branches-add.component.html',
  styleUrls: ['./admin-branches-add.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminBranchesAddComponent implements OnInit {

  form: FormGroup;

  private _companyId: string;

  constructor(private readonly _adminBranchService: AdminBranchService,
              private _formBuilder: FormBuilder,
              private _snackBar: MatSnackBar,
              private _router: Router) {
    this._companyId = '';
  }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: ['', Validators.required],
      phone: [''],
      companyId: ['', Validators.required],
      address: ['']
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const request: Partial<RequestAdminBranch> = this._setRequest();
      this._adminBranchService.create(request).subscribe(data => {
        if (data.header.code === 200) {
          this._router.navigate(['admin/branches']);
        } else {
          this._snackBar.open(data.header.description);
        }
      }, error => {
        this._snackBar.open('NO FUE POSIBLE PROCESAR SU PETICION');
      });
    }
  }

  onCompanySelected(companyId: string) {
    console.log(`Company Id => ${companyId}`);
    this.form.controls.companyId.setValue(companyId);
  }

  private _setRequest(): Partial<RequestAdminBranch> {
    return {
      name: this.form.controls.name.value,
      address: this.form.controls.address.value,
      phone: this.form.controls.phone.value,
      companyId: this.form.controls.companyId.value,
      status: BaseStatus.active
    };
  }

  return() {
    this._router.navigate(['admin/branches']);
  }
}
