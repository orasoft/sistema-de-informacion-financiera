import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@sif/module/ui/theme/fuse';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RequestAdminBranch } from '@sif/shared/message/request';
import { ActivatedRoute, Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { AdminBranch } from '@sif/shared/message/response';
import { AdminBranchService } from '../../services/admin-branch.service';
import { BaseStatus } from '@sif/shared/types';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'branches-edit',
  templateUrl: './admin-branches-edit.component.html',
  styleUrls: ['./admin-branches-edit.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AdminBranchesEditComponent implements OnInit {

  form: FormGroup;

  companyIdSelectedOrigin: string;
  data : AdminBranch;
  constructor(
              private readonly _adminBranchService: AdminBranchService,
              private _formBuilder: FormBuilder,
              private _snackBar: MatSnackBar,
              private _router: Router,
              private _activatedRoute: ActivatedRoute) {
    this.data = JSON.parse(this._activatedRoute.snapshot.queryParams.row);
  }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: [this.data.name, Validators.required],
      phone: [this.data.phone],
      address: [this.data.address],
      companyId: [this.data.company.id],
      status: [this.data.status || BaseStatus.disable]
    });

    this.companyIdSelectedOrigin = this.data.company.id;
  }

  onCompanySelected(companyId: string) {
    console.log(`Company Selected => ${companyId}`);
    this.form.controls.companyId.setValue(companyId);
  }

  onSubmit() {
    if (this.form.valid) {
      const request: Partial<RequestAdminBranch> = {
        id: this.data.id,
        name: this.form.controls.name.value,
        phone: this.form.controls.phone.value,
        address: this.form.controls.address.value,
        status: this.form.controls.status.value,
        companyId: this.form.controls.companyId.value
      };
      this._adminBranchService.update(request).subscribe(data => {
        if (data.header.code === 200) {
          this._router.navigate(['admin/branches']);
        } else {
          this._snackBar.open(data.header.description);
        }
      }, error => {
        this._snackBar.open('NO FUE POSIBLE PROCESAR SU PETICION');
      });
    }
  }

  return() {
    this._router.navigate(['admin/branches']);
  }
}
