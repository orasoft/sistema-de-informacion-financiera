export enum BaseStatus {
  active = 'A',
  inactive = 'I',
  disable = 'D'
}
