export interface RequestAuthLogin {
  username: string;
  password: string;
}
