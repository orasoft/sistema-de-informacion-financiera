import { BaseStatus } from '@sif/shared/types';

export class RequestAdminUser{
  id: string;
  name: string;
  username: string;
  password: string;
  avatar: string;
  email: string;
  status: BaseStatus
}
