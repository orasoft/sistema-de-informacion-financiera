

export interface RequestAdminUserRoleBranch {
  id: string;
  userId: string;
  roleId: string;
  branchId: string;
}
