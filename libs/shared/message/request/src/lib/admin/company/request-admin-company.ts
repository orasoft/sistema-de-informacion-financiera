import { BaseStatus, TypeCompany } from '@sif/shared/types';

export class RequestAdminCompany {
  id: string;
  name: string;
  nit: string;
  dui: string;
  nrc: string;
  type: TypeCompany;
  address: string;
  phone: string;
  status: BaseStatus
}
