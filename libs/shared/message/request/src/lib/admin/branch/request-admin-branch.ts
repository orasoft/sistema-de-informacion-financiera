import { BaseStatus } from '@sif/shared/types';

export interface RequestAdminBranch {
  id: string;
  companyId: string;
  name: string;
  phone: string;
  address: string;
  status: BaseStatus;
}
