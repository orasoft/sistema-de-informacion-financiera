import { BaseStatus } from '@sif/shared/types';

export class RequestAdminRole {
  id: string;
  name: string;
  status: BaseStatus;
}
