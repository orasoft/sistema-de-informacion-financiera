export interface AdminRole {
  id?: string;
  name?: string;
  status?: string;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
}
