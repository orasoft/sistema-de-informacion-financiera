import { IMessageResponseHeader, ResponseGenericCatalog } from '@sif/shared/message/base';
import { AdminRoleCatalog } from './admin-role-catalog';


export class ResponseAdminRoleCatalog extends ResponseGenericCatalog<Partial<AdminRoleCatalog>[]> {
  public header: IMessageResponseHeader;
  public catalog: Partial<AdminRoleCatalog>[] | [];

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAdminRoleCatalog.name;
  }
}
