import { IMessageResponseHeader, ResponseGenericData } from '@sif/shared/message/base';
import { AdminRole } from './admin-role';


export class ResponseAdminRole extends ResponseGenericData<Partial<AdminRole>[]> {
  public header: IMessageResponseHeader;
  public body: Partial<AdminRole>[] | [];

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAdminRole.name;
  }
}
