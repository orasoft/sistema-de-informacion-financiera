import { IMessageResponseHeader, ResponseGenericData } from '@sif/shared/message/base';
import { AdminBranch } from './admin-branch';

export class ResponseAdminBranch extends ResponseGenericData<Partial<AdminBranch>[]> {
  public header: IMessageResponseHeader;
  public data: Partial<AdminBranch>[] | [];

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAdminBranch.name
  }
}
