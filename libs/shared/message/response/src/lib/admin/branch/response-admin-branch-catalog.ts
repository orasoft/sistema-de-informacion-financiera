import { IMessageResponseHeader, ResponseGenericCatalog } from '@sif/shared/message/base';
import { AdminBranchCatalog } from './admin-branch-catalog';


export class ResponseAdminBranchCatalog extends ResponseGenericCatalog<Partial<AdminBranchCatalog>[]> {
  public header: IMessageResponseHeader;
  public catalog: Partial<AdminBranchCatalog>[] | [];

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAdminBranchCatalog.name;
  }
}
