import { BaseStatus } from '@sif/shared/types';
import { AdminCompany } from '../../../lib/admin/company/admin-company';

export interface AdminBranch {
  id?: string;
  company: AdminCompany;
  name?: string;
  phone?: string;
  address?: string;
  status?: BaseStatus;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
}
