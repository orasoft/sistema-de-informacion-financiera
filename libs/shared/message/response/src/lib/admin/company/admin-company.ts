import { BaseStatus } from '@sif/shared/types';

export class AdminCompany {
  id?: string;
  name: string;
  nit: string;
  dui?: string;
  nrc?: string;
  type?: string;
  address?: string;
  logo?: string;
  phone?: string;
  status: BaseStatus;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
}
