import { IMessageResponseHeader, ResponseGenericCatalog } from '@sif/shared/message/base';
import { AdminCompanyCatalog } from './admin-company-catalog';

export class ResponseAdminCompanyCatalog extends ResponseGenericCatalog<Partial<AdminCompanyCatalog>[]> {
  public header: IMessageResponseHeader;
  public catalog: Partial<AdminCompanyCatalog>[] | [];

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAdminCompanyCatalog.name;
  }
}
