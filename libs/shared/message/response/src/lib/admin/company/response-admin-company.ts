import { IMessageResponseHeader, ResponseGenericData } from '@sif/shared/message/base';
import { AdminCompany } from './admin-company';

export class ResponseAdminCompany extends ResponseGenericData<Partial<AdminCompany>[]> {
  public header: IMessageResponseHeader;
  public data: Partial<AdminCompany>[] | [];

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAdminCompany.name;
  }
}
