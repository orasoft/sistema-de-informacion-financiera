import { AdminBranch, AdminRole, AdminUser } from '../../..';

export interface AdminUserRoleBranch{
  id?: string;
  user: AdminUser;
  role: AdminRole;
  branch: AdminBranch;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
}
