import { IMessageResponseHeader, ResponseGenericData } from '@sif/shared/message/base';
import { AdminUserRoleBranch } from './admin-user-role-branch';


export class ResponseAdminUserRoleBranch extends ResponseGenericData<Partial<AdminUserRoleBranch>[]> {
  public header: IMessageResponseHeader;
  public body: Partial<AdminUserRoleBranch>[] | [];

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAdminUserRoleBranch.name;
  }
}
