import { IMessageResponseHeader, ResponseGenericCatalog } from '@sif/shared/message/base';
import { AdminUserCatalog } from './admin-user-catalog';


export class ResponseAdminUserCatalog extends ResponseGenericCatalog<Partial<AdminUserCatalog>[]> {
  public header: IMessageResponseHeader;
  public catalog: Partial<AdminUserCatalog>[] | [];

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAdminUserCatalog.name;
  }
}
