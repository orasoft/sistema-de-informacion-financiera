export interface AdminUser {
  id?: string;
  name?: string;
  username?: string;
  password?: string;
  avatar?: string;
  email?: string;
  status?: string;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
}
