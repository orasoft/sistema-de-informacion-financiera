import { IMessageResponseHeader, ResponseGenericData } from '@sif/shared/message/base';
import { AdminUser } from './admin-user';


export class ResponseAdminUser extends ResponseGenericData<Partial<AdminUser>[]> {
  public header: IMessageResponseHeader;
  public body: Partial<AdminUser>[] | [];

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAdminUser.name;
  }
}
