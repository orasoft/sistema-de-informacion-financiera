import { IMessageResponseHeader, ResponseGenericData } from '@sif/shared/message/base';
import { AuthLogin } from './auth-login';

export class ResponseAuthLogin extends ResponseGenericData<Partial<AuthLogin>> {
  public header: IMessageResponseHeader;
  public data: Partial<AuthLogin>;

  constructor(code: number, description: string) {
    super(code, description);
    this.header.object = ResponseAuthLogin.name
  }
}
