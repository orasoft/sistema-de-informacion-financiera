import { IMessageResponseHeader, IResponseGeneric } from '../interfaces';
import { TypeMessage } from '../enums';


export abstract class ResponseGenericCatalog<T> implements IResponseGeneric {
  catalog: T;
  header: Partial<IMessageResponseHeader>;

  protected constructor(code: number, description: string) {
    this.header = {
      code,
      description,
      transaction: Date.now(),
      type: TypeMessage.response
    };
  }

  setCatalog(records: T): void {
    this.catalog = records;
  }
}
