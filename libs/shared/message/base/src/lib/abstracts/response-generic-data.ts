import { IMessageResponseHeader, IResponseGeneric } from '../interfaces';
import { TypeMessage } from '../enums';


export abstract class ResponseGenericData<T> implements IResponseGeneric {
  data: T;
  header: Partial<IMessageResponseHeader>;

  protected constructor(code: number, description: string) {
    this.header = {
      code,
      description,
      transaction: Date.now(),
      type: TypeMessage.response
    };
  }

  setData(records: T): void {
    this.data = records;
  }
}
