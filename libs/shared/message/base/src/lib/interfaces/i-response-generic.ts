import { IMessageResponseHeader } from './i-message-response-header';

export interface IResponseGeneric {
  header: Partial<IMessageResponseHeader>;
}
