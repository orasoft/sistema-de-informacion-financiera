import { TypeMessage } from '../enums';

export interface IMessageResponseHeader {
  code: number;
  description: string;
  transaction: number;
  type: TypeMessage.response;
  object: string;
}
