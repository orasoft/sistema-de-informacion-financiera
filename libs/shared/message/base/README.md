# shared-message-base

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test shared-message-base` to execute the unit tests via [Jest](https://jestjs.io).
