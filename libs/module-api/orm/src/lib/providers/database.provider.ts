import { createConnection } from 'typeorm';
import { BranchEntity, CompanyEntity, MenuEntity, ModuleEntity, RoleEntity, UserEntity, UserRoleBranchEntity } from '../entities';

export const databaseProvider = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () => {
      return await createConnection({
        type: 'oracle',
        host: '10.70.95.61',
        port: 1521,
        username: 'USER_SIF',
        password: 'erpsif$2019',
        sid: 'dev02',
        schema: 'USER_SIF',
        entities: [
          BranchEntity,
          CompanyEntity,
          MenuEntity,
          RoleEntity,
          UserEntity,
          ModuleEntity,
          UserRoleBranchEntity,
        ],
        synchronize: true,
        logging: true
      });
    }
  }
];
