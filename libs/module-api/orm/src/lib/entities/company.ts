import { EntitySchema } from 'typeorm';
import { ICompany } from '../interfaces';
import { BaseStatus, TypeCompany } from '@sif/shared/types';

export const CompanyEntity = new EntitySchema<ICompany>({
  name: 'Company',
  tableName: 'COMPANY',
  columns: {
    id: {
      name: 'ID',
      type: String,
      primary: true,
      generated: 'uuid'
    },
    name: {
      name: 'NAME',
      type: 'varchar'
    },
    nit: {
      name: 'NIT',
      type: 'varchar',
      nullable: true
    },
    dui: {
      name: 'DUI',
      type: 'varchar',
      nullable: true
    },
    nrc: {
      name: 'NRC',
      type: 'varchar',
      nullable: true
    },
    type: {
      name: 'TYPE',
      type: 'varchar',
      enum: TypeCompany
    },
    address: {
      name: 'ADDRESS',
      type: 'varchar',
      nullable: true
    },
    logo: {
      name: 'LOGO',
      type: 'varchar',
      nullable: true
    },
    phone: {
      name: 'PHONE',
      type: 'varchar',
      nullable: true
    },
    status: {
      name: 'STATUS',
      type: 'varchar',
      enum: BaseStatus
    },
    createAt: {
      name: 'CREATED_AT',
      type: 'date',
      createDate: true,
      nullable: false
    },
    createBy: {
      name: 'CREATED_BY',
      type: String,
      nullable: false
    },
    updatedAt: {
      name: 'UPDATED_AT',
      type: 'date',
      updateDate: true,
      nullable: true
    },
    updatedBy: {
      name: 'UPDATED_BY',
      type: String,
      nullable: true
    },
    version: {
      name: 'VERSION',
      type: Number,
      version: true
    }
  },
  relations: {
    branches: {
      type: 'one-to-many',
      target: 'Branch'
    }
  }
});
