import { EntitySchema } from 'typeorm';
import { IRole } from '../interfaces';
import { BaseStatus } from '@sif/shared/types';

export const RoleEntity = new EntitySchema<IRole>({
  name: 'Role',
  tableName: 'ROLE',
  columns: {
    id: {
      name: 'ID',
      type: String,
      primary: true,
      generated: 'uuid'
    },
    name: {
      name: 'NAME',
      type: 'varchar'
    },
    status: {
      name: 'STATUS',
      type: 'varchar',
      enum: BaseStatus
    },
    createAt: {
      name: 'CREATED_AT',
      type: 'date',
      createDate: true,
      nullable: false
    },
    createBy: {
      name: 'CREATED_BY',
      type: String,
      nullable: false
    },
    updatedAt: {
      name: 'UPDATED_AT',
      type: 'date',
      updateDate: true,
      nullable: false
    },
    updatedBy: {
      name: 'UPDATED_BY',
      type: String,
      nullable: false
    },
    version: {
      name: 'VERSION',
      type: Number,
      version: true
    }
  },
  relations: {
    userRoleBranch: {
      type: 'one-to-many',
      target: 'UserRoleBranch',
      inverseSide: 'role'
    }
  }
});

