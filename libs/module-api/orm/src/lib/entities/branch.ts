import { EntitySchema } from 'typeorm';
import { IBranch } from '../interfaces';
import { BaseStatus } from '@sif/shared/types';

export const BranchEntity = new EntitySchema<IBranch>({
  name: 'Branch',
  tableName: 'BRANCH',
  columns: {
    id: {
      name: 'ID',
      type: String,
      primary: true,
      generated: 'uuid'
    },
    name: {
      name: 'NAME',
      type: 'varchar'
    },
    phone: {
      name: 'PHONE',
      type: 'varchar',
      nullable: true
    },
    address: {
      name: 'ADDRESS',
      type: 'varchar',
      nullable: true
    },
    status: {
      name: 'STATUS',
      type: 'varchar',
      enum: BaseStatus
    },
    createAt: {
      name: 'CREATED_AT',
      type: 'date',
      createDate: true,
      nullable: false
    },
    createBy: {
      name: 'CREATED_BY',
      type: String,
      nullable: false
    },
    updatedAt: {
      name: 'UPDATED_AT',
      type: 'date',
      updateDate: true,
      nullable: false
    },
    updatedBy: {
      name: 'UPDATED_BY',
      type: String,
      nullable: false
    },
    version: {
      name: 'VERSION',
      type: Number,
      version: true
    }
  },
  relations: {
    company: {
      type: 'many-to-one',
      target: 'Company',
      joinColumn: {
        name: 'COMPANY_ID'
      }
    },
    userRoleBranch: {
      type: 'one-to-many',
      target: 'UserRoleBranch',
      inverseSide: 'branch'
    }
  }
});
