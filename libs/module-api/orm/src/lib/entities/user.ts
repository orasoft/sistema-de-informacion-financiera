import { EntitySchema } from 'typeorm';
import { IUser } from '../interfaces';
import { BaseStatus } from '@sif/shared/types';

export const UserEntity = new EntitySchema<IUser>({
  name: 'User',
  tableName: 'USER',
  columns: {
    id: {
      name: 'ID',
      type: String,
      primary: true,
      generated: 'uuid'
    },
    name: {
      name: 'NAME',
      type: 'varchar'
    },
    username: {
      name: 'USERNAME',
      type: 'varchar',
      unique: true
    },
    password: {
      name: 'PASSWORD',
      type: 'varchar'
    },
    avatar: {
      name: 'AVATAR',
      type: 'varchar',
      nullable: true
    },
    email: {
      name: 'EMAIL',
      type: 'varchar',
      unique: true
    },
    status: {
      name: 'STATUS',
      type: 'varchar',
      enum: BaseStatus
    },
    createAt: {
      name: 'CREATED_AT',
      type: 'date',
      createDate: true,
      nullable: false
    },
    createBy: {
      name: 'CREATED_BY',
      type: String,
      nullable: false
    },
    updatedAt: {
      name: 'UPDATED_AT',
      type: 'date',
      updateDate: true,
      nullable: false
    },
    updatedBy: {
      name: 'UPDATED_BY',
      type: String,
      nullable: false
    },
    version: {
      name: 'VERSION',
      type: Number,
      version: true
    }
  },
  relations: {
    userRoleBranch: {
      type: 'one-to-many',
      target: 'UserRoleBranch',
      inverseSide: 'user'
    }
  }
});
