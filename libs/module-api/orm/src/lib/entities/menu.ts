import { EntitySchema } from 'typeorm';
import { IMenu } from '../interfaces';
import { BaseStatus, TypeOption } from '@sif/shared/types';

export const MenuEntity = new EntitySchema<IMenu>({
  name: 'Menu',
  tableName: 'MENU',
  columns: {
    id: {
      name: 'ID',
      type: String,
      primary: true,
      generated: 'uuid'
    },
    name: {
      name: 'NAME',
      type: 'varchar'
    },
    url: {
      name: 'URL',
      type: 'varchar'
    },
    icon: {
      name: 'ICON',
      type: 'varchar'
    },
    principal: {
      name: 'PRINCIPAL',
      type: 'varchar'
    },
    typeOption: {
      name: 'TYPE_OPTION',
      type: 'varchar',
      enum: TypeOption
    },
    status: {
      name: 'STATUS',
      type: 'varchar',
      enum: BaseStatus
    },
    createAt: {
      name: 'CREATED_AT',
      type: 'date',
      createDate: true,
      nullable: false
    },
    createBy: {
      name: 'CREATED_BY',
      type: String,
      nullable: false
    },
    updatedAt: {
      name: 'UPDATED_AT',
      type: 'date',
      updateDate: true,
      nullable: false
    },
    updatedBy: {
      name: 'UPDATED_BY',
      type: String,
      nullable: false
    },
    version: {
      name: 'VERSION',
      type: Number,
      version: true
    }
  }
});
