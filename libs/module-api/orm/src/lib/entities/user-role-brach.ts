import { EntitySchema } from 'typeorm';
import { IUserRoleBranch } from '../interfaces';


export const UserRoleBranchEntity = new EntitySchema<IUserRoleBranch>({
  name: 'UserRoleBranch',
  tableName: 'USER_ROLE_BRANCH',
  columns: {
    id: {
      name: 'ID',
      type: String,
      primary: true,
      generated: 'uuid'
    },
    createAt: {
      name: 'CREATED_AT',
      type: 'date',
      createDate: true,
      nullable: false
    },
    createBy: {
      name: 'CREATED_BY',
      type: String,
      nullable: false
    },
    updatedAt: {
      name: 'UPDATED_AT',
      type: 'date',
      updateDate: true,
      nullable: false
    },
    updatedBy: {
      name: 'UPDATED_BY',
      type: String,
      nullable: false
    },
    version: {
      name: 'VERSION',
      type: Number,
      version: true
    }
  },
  relations: {
    user: {
      type: 'many-to-one',
      target: 'User',
      joinColumn: true,
    },
    role: {
      type: 'many-to-one',
      target: 'Role',
      joinColumn: true
    },
    branch: {
      type: 'many-to-one',
      target: 'Branch',
      joinColumn: true
    }
  }
});
