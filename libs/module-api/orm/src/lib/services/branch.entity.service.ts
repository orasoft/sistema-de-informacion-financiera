import { Inject, Injectable, Logger } from '@nestjs/common';
import { IBranch } from '../interfaces';
import { BranchEntity } from '../entities';


@Injectable()
export class BranchEntityService {
  constructor(@Inject('DATABASE_CONNECTION') private connection) {
  }

  async create(record: Partial<IBranch>) {
    return this.connection.manager.createQueryBuilder().insert().into(BranchEntity).values([
      {
        name: record.name,
        company: record.company,
        address: record.address,
        phone: record.phone,
        status: record.status,
        createBy: 'system',
        updatedBy: 'system'
      }
    ]).execute();
  }

  async findAll() {
    return this.connection
      .manager
      .createQueryBuilder()
      .select('a')
      .from('BRANCH', 'a')
      .getMany();
  }

  async getAllWithCompany() {
    return this.connection.manager
      .createQueryBuilder()
      .select('b')
      .from('BRANCH', 'b')
      .innerJoinAndMapOne('b.company', 'Company', 'c', `"b".COMPANY_ID = "c".ID`)
      .getMany();
  }

  async findById(id: string) {
    return this.connection
      .manager
      .createQueryBuilder()
      .select('c')
      .from('BRANCH', 'c')
      .where('ID = :id', { id })
      .getOne();
  }

  async findByCompanyId(companyId: string) {
    return this.connection.manager
      .createQueryBuilder()
      .select('b')
      .from('BRANCH', 'b')
      .innerJoinAndMapOne('b.company', 'Company', 'c', `"b".COMPANY_ID = "c".ID`)
      .where('COMPANY_ID = :companyId', { companyId })
      .getMany();
  }

  async findByIdWithCompany(branchId: string): Promise<IBranch> {
    return await this.connection.getRepository(BranchEntity).findOne({id: branchId}, {relations: ['company']});
  }

  async getCatalog(): Promise<IBranch[]> {
    Logger.log(`Catalog Branch Entity Service`);
    return this.connection
      .manager
      .createQueryBuilder()
      .select('b')
      .addSelect(['ID, NAME'])
      .from('BRANCH', 'b')
      .getMany();
  }

  async getCatalogByCompanyId(companyId: string): Promise<IBranch[]> {
    Logger.log(`Catalog Branch Entity Service`);
    return this.connection
      .manager
      .createQueryBuilder()
      .select('b')
      .addSelect(['ID, NAME'])
      .from('BRANCH', 'b')
      .where('COMPANY_ID = :companyId', { companyId })
      .getMany();
  }

  async update(record: Partial<IBranch>) {
    return this.connection.manager.createQueryBuilder().update(BranchEntity).set({
      name: record.name,
      phone: record.phone,
      address: record.address,
      company: record.company,
      status: record.status,
      updatedBy: 'system'
    }).where('ID = :id', { id: record.id }).execute();
  }
}
