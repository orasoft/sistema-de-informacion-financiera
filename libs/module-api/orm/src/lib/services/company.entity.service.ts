import { Inject, Injectable } from '@nestjs/common';
import { ICompany } from '../interfaces';
import { CompanyEntity } from '../entities';


@Injectable()
export class CompanyEntityService {
  constructor(@Inject('DATABASE_CONNECTION') private connection) {
  }

  async create(record: Partial<ICompany>) {
    return this.connection.manager.createQueryBuilder().insert().into(CompanyEntity).values([
      {
        name: record.name,
        type: record.type,
        phone: record.phone || null,
        nrc: record.nrc || null,
        address: record.address || null,
        dui: record.dui || null,
        logo: record.logo || null,
        nit: record.nit || null,
        status: record.status,
        createBy: 'system',
        updatedBy: 'system'
      }
    ]).execute();
  }

  async findAll(): Promise<ICompany[]> {
    return await this.connection
      .manager
      .createQueryBuilder()
      .select('c')
      .from('Company', 'c')
      .getMany();
  }

  async findAllWithBranches(): Promise<ICompany[]> {
    return await this.connection.getRepository(CompanyEntity).find({relations: ['branch']});
  }

  async findById(id: string): Promise<ICompany> {
    return this.connection
      .manager
      .createQueryBuilder()
      .select('c')
      .from('Company', 'c')
      .where('c.id = :id', { id })
      .limit(1)
      .getOne();
  }

  async getCatalog(): Promise<ICompany[]> {
    return this.connection
      .manager
      .createQueryBuilder()
      .select('c')
      .addSelect(['ID, NAME'])
      .from('COMPANY', 'c')
      .getMany();
  }

  async update(record: ICompany) {
    return this.connection
      .manager
      .createQueryBuilder()
      .update(CompanyEntity)
      .set({
        name: record.name,
        type: record.type,
        phone: record.phone || null,
        nrc: record.nrc || null,
        address: record.address || null,
        dui: record.dui || null,
        logo: record.logo || null,
        nit: record.nit || null,
        status: record.status,
        updatedBy: record.updatedBy
      }).where('id = :id', { id: record.id })
      .execute();
  }
}
