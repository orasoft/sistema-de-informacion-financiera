import { Inject, Injectable } from '@nestjs/common';
import { IRole } from '../interfaces';
import { RoleEntity } from '../entities'

@Injectable()
export class RoleEntityService {

  constructor(@Inject('DATABASE_CONNECTION') private connection) {
  }

  async create(record: Partial<IRole>) {
    return this.connection.manager.createQueryBuilder().insert().into(RoleEntity).values([
      {
        name: record.name,
        status: record.status,
        createBy: 'system',
        updatedBy: 'system'
      }
    ]).execute();
  }

  async findAll(): Promise<IRole[]> {
    return await this.connection
      .manager
      .createQueryBuilder()
      .select('r')
      .from('Role', 'r')
      .getMany();
  }

  async findById(id: string): Promise<IRole> {
    return this.connection
      .manager
      .createQueryBuilder()
      .select('r')
      .from('Role', 'r')
      .where('r.id = :id', {id})
      .limit(1)
      .getOne();
  }

  async getCatalog(): Promise<IRole[]> {
    return this.connection
      .manager
      .createQueryBuilder()
      .select('r')
      .addSelect(['ID, NAME'])
      .from( 'ROLE', 'r')
      .getMany();
  }

  async update(record: IRole) {
    return this.connection
      .manager
      .createQueryBuilder()
      .update(RoleEntity)
      .set({
        name: record.name,
        status: record.status,
        updatedBy: record.updatedBy
      }).where('id = :id', {id: record.id})
      .execute();
  }

}
