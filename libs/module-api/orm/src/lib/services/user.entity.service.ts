import { Inject, Injectable } from '@nestjs/common';
import { IUser} from '../interfaces';
import {UserEntity} from '../entities'


@Injectable()
export class UserEntityService {
  constructor(@Inject('DATABASE_CONNECTION') private connection) {
  }

  async create(record: Partial<IUser>) {
    return this.connection.manager.createQueryBuilder().insert().into(UserEntity).values([
      {
        name: record.name,
        username: record.username,
        password: record.password,
        email: record.email,
        avatar: record.avatar,
        status: record.status,
        createBy: 'system',
        updatedBy: 'system'
      }
    ]).execute();
  }

  async findAll(): Promise<IUser[]> {
    return await this.connection
      .manager
      .createQueryBuilder()
      .select('u')
      .from('User', 'u')
      .getMany();
  }

  async findById(id: string): Promise<IUser> {
    return this.connection
      .manager
      .createQueryBuilder()
      .select('u')
      .from('User', 'u')
      .where('u.id = :id', {id})
      .limit(1)
      .getOne();
  }

  async findByUsername(username: string): Promise<IUser> {
    return this.connection
      .manager
      .createQueryBuilder()
      .select('u')
      .from('User', 'u')
      .where('u.username = :username', {username})
      .limit(1)
      .getOne();
  }

  async getCatalog(): Promise<IUser[]> {
    return this.connection
      .manager
      .createQueryBuilder()
      .select('u')
      .addSelect(['ID, USERNAME'])
      .from( 'USER', 'u')
      .getMany();
  }

  async update(record: IUser) {
    return this.connection
      .manager
      .createQueryBuilder()
      .update(UserEntity)
      .set({
        name: record.name,
        username: record.username,
        email: record.email,
        avatar: record.avatar,
        status: record.status,
        updatedBy: record.updatedBy
      }).where('id = :id', {id: record.id})
      .execute();
  }

  async updatePass(record: IUser) {
    return this.connection
      .manager
      .createQueryBuilder()
      .update(UserEntity)
      .set({
        password: record.password,
      }).where('id = :id', {id: record.id})
      .execute();
  }
}
