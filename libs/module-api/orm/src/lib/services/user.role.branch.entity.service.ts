import { Inject } from '@nestjs/common';
import { Injectable } from '@angular/core';
import { IUserRoleBranch } from '../interfaces';
import { UserRoleBranchEntity } from '../entities';


@Injectable()
export class UserRoleBranchEntityService {
  constructor(@Inject('DATABASE_CONNECTION') private connection) {
  }

  async create(record: Partial<IUserRoleBranch>) {
    return this.connection.manager.createQueryBuilder().insert().into(UserRoleBranchEntity).values([
      {
        user: record.user,
        role: record.role,
        branch: record.branch,
        createBy: 'system',
        updatedBy: 'system'
      }
    ]).execute();
  }

  async findById(id: string): Promise<IUserRoleBranch> {
    return await this.connection.getRepository(UserRoleBranchEntity).findOne({ id: id }, { relations: ['user', 'role', 'branch'] })
  }

  async findByUserIdAndBranchId(userId: string, branchId: string): Promise<IUserRoleBranch[]> {
    return await this.connection.getRepository(UserRoleBranchEntity).find({ where: {user: {id: userId}, branch: { id: branchId }}, relations: ['user', 'role', 'branch']});
  }

  async findByUserId(userId: string): Promise<IUserRoleBranch[]> {
    return await this.connection.getRepository(UserRoleBranchEntity).find({ where: {user: {id: userId}}, relations: ['user', 'role', 'branch']});
  }

  async delete(id: string): Promise<any> {
    return await this.connection.getRepository(UserRoleBranchEntity).delete({id: id});
  }
}
