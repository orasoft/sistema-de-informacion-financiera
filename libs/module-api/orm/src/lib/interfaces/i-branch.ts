import { ICompany } from './i-company';
import { BaseStatus } from '@sif/shared/types';
import { IUserRoleBranch } from './i-user-role-branch';

export interface IBranch {
  id?: string;
  company: ICompany;
  name?: string;
  phone?: string;
  address?: string;
  status?: BaseStatus;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
  userRoleBranch?: IUserRoleBranch[];
}
