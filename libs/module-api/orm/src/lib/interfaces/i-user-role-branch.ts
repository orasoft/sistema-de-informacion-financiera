import { IBranch, IRole, IUser } from '@sif/module-api/orm';

export interface IUserRoleBranch {
  id?: string;
  user?: IUser;
  role?: IRole;
  branch?: IBranch;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
}
