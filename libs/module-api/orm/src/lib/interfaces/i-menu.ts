export interface IMenu {
  id?: string;
  name?: string;
  url?: string;
  icon?: string;
  principal?: string;
  typeOption?: string;
  status?: string;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
}
