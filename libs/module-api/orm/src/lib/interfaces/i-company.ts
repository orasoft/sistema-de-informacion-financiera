import { IBranch } from './i-branch';
import { BaseStatus } from '@sif/shared/types';

export interface ICompany {
  id?: string;
  name: string;
  nit: string;
  dui?: string;
  nrc?: string;
  type?: string;
  address?: string;
  logo?: string;
  phone?: string;
  status: BaseStatus;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
  branches: IBranch[];
}
