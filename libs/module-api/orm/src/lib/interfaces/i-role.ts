 import { IUser } from './i-user';
 import { IUserRoleBranch } from './i-user-role-branch';

export interface IRole {
  id?: string;
  name?: string;
  status?: string;
  createAt?: Date;
  createBy?: string;
  updatedAt?: Date;
  updatedBy?: string;
  version?: number;
  users?: IUser[];
  userRoleBranch?: IUserRoleBranch[];
}
