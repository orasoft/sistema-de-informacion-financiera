import { Module } from '@nestjs/common';
import { databaseProvider } from './providers/database.provider';
import {
  BranchEntityService,
  CompanyEntityService,
  RoleEntityService,
  UserEntityService,
  UserRoleBranchEntityService
} from './services';

const providers = [
  BranchEntityService,
  CompanyEntityService,
  UserEntityService,
  RoleEntityService,
  UserRoleBranchEntityService
];

@Module({
  exports: [
    ...databaseProvider,
    ...providers
  ],
  imports: [],
  providers: [
    ...databaseProvider,
    ...providers
  ],
  controllers: []
})
export class OrmModule {
}
