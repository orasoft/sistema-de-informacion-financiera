import { UserRoleBranchDomainService } from './services/user-role-branch.domain.service';
import { Module } from '@nestjs/common';
import { OrmModule } from '@sif/module-api/orm';


const providers = [
  UserRoleBranchDomainService
];

@Module({
  exports: [
    ...providers
  ],
  imports: [
    OrmModule
  ],
  providers: [
    ...providers
  ],
  controllers: []
})
export class UserRoleBranchDomainModule {
}
