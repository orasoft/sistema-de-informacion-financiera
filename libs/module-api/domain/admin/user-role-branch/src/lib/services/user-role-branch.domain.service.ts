import { HttpStatus, Injectable, Logger, NotFoundException } from '@nestjs/common';
import {
  BranchEntityService,
  IUserRoleBranch,
  RoleEntityService,
  UserEntityService,
  UserRoleBranchEntityService
} from '@sif/module-api/orm';
import { RequestAdminUserRoleBranch } from '@sif/shared/message/request';
import { AdminUserRoleBranch, ResponseAdminUserRoleBranch } from '@sif/shared/message/response';


@Injectable()
export class UserRoleBranchDomainService {

  constructor(private readonly useRoleBranchEntityService: UserRoleBranchEntityService,
              private readonly userEntityService: UserEntityService,
              private readonly roleEntityService: RoleEntityService,
              private readonly branchEntityService: BranchEntityService) {
  };

  static IUserRoleBranchToAdminUserRoleBranch(record: IUserRoleBranch): AdminUserRoleBranch {
    return {
      id: record.id,
      user: record.user,
      role: record.role,
      branch: record.branch,
      createAt: record.createAt,
      createBy: record.createBy,
      updatedAt: record.updatedAt,
      updatedBy: record.updatedBy,
      version: record.version
    };
  }

  async create(requestAdminUserRoleBranch: Partial<RequestAdminUserRoleBranch>): Promise<ResponseAdminUserRoleBranch> {
    const user = await this.userEntityService.findById(requestAdminUserRoleBranch.userId);
    const role = await this.roleEntityService.findById(requestAdminUserRoleBranch.roleId);
    const branch = await this.branchEntityService.findById(requestAdminUserRoleBranch.branchId);
    const record: IUserRoleBranch = {
      user: user,
      role: role,
      branch: branch,
    };
    const insertResult = await this.useRoleBranchEntityService.create(record);
    const recordCreatedId = insertResult.identifiers[0];
    Logger.error(`insertResult ===> ` + insertResult.identifiers[0]);
    Logger.error(`recordCreatedId ===> ` + recordCreatedId);
    return this.getById(recordCreatedId.id);
  }

  async getById(id: string): Promise<ResponseAdminUserRoleBranch> {
    return new Promise<ResponseAdminUserRoleBranch>( (resolve => {
      Logger.error(`idRoleUserBranch ===> ` + id);
      this.useRoleBranchEntityService.findById(id).then(data => {
        const records: AdminUserRoleBranch[] = [];
        records.push(UserRoleBranchDomainService.IUserRoleBranchToAdminUserRoleBranch(data));
        if(records.length > 0){
          const response = new ResponseAdminUserRoleBranch(HttpStatus.OK, 'TRANSACCION EXITOSA');
          response.setData(records);
          resolve(response);
        } else {
          const notFound =  new ResponseAdminUserRoleBranch(HttpStatus.NOT_FOUND, 'REGISTRO NO ENCONTRADO');
          notFound.setData(records);
          resolve(notFound);
        }
      }).catch( error => {
        Logger.error(`response error ===> ` + error);
      });
    }))
  }

  async getByUserId(userId: string, branchId: string): Promise<ResponseAdminUserRoleBranch> {
    return new Promise<ResponseAdminUserRoleBranch>( (resolve => {
      this.useRoleBranchEntityService.findByUserIdAndBranchId(userId, branchId).then( data => {
        const records: AdminUserRoleBranch[] = [];
        data.forEach(record => {
          Logger.error(`response findByUserIdAndBranchId ===> ` + record.id);
          Logger.error(`response findByUserIdAndBranchId user ===> ` + record.user.id);
          records.push(UserRoleBranchDomainService.IUserRoleBranchToAdminUserRoleBranch(record));
        });

        const response = new ResponseAdminUserRoleBranch(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      });
    }));
  }

  async delete(id: string): Promise<ResponseAdminUserRoleBranch> {
    return new Promise<ResponseAdminUserRoleBranch> ((resolve => {
      this.useRoleBranchEntityService.delete(id).then( data => {
        const response = new ResponseAdminUserRoleBranch(HttpStatus.OK, 'TRANSACCION EXITOSA');
        resolve(response);
      })
    }));
  }
}
