import { HttpStatus, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { IUser, UserEntityService } from '@sif/module-api/orm';
import {
  AdminUser,
  ResponseAdminUser
} from '@sif/shared/message/response';
import { RequestAdminChangePassword, RequestAdminUser } from '@sif/shared/message/request';
import { AdminUserCatalog } from '@sif/shared/message/response';
import { ResponseAdminUserCatalog } from '@sif/shared/message/response';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class UserDomainService {

  constructor(private readonly userEntityService: UserEntityService) {
  };

  static IUserToAdminUser(record: IUser): AdminUser {
    return {
      id: record.id,
      name: record.name,
      username: record.username,
      email: record.email,
      avatar: record.avatar,
      status: record.status,
      createAt: record.createAt,
      createBy: record.createBy,
      updatedAt: record.updatedAt,
      updatedBy: record.updatedBy,
      version: record.version
    };
  }

  static IUserToAdminUserCreate(record: IUser, temporalPassword?: string): AdminUser {
    return {
      id: record.id,
      name: record.name,
      username: record.username,
      password: temporalPassword,
      email: record.email,
      avatar: record.avatar,
      status: record.status,
      createAt: record.createAt,
      createBy: record.createBy,
      updatedAt: record.updatedAt,
      updatedBy: record.updatedBy,
      version: record.version
    };
  }

  static requestAdminUserToUser(requestAdminUser: Partial<RequestAdminUser>): Partial<IUser> {
    return {
      id: requestAdminUser.id || undefined,
      name: requestAdminUser.name,
      username: requestAdminUser.username,
      password: requestAdminUser.password,
      email: requestAdminUser.email,
      avatar: requestAdminUser.avatar,
      status: requestAdminUser.status
    }
  }

  static IUserCatalogToAdminUserCatalog(record: IUser): AdminUserCatalog {
    return {
      id: record.id,
      name: record.name
    }
  }

  async getAll(): Promise<ResponseAdminUser> {
    return new Promise<ResponseAdminUser> ( resolve => {
      this.userEntityService.findAll().then(data => {
        const records: AdminUser[] = [];
        data.forEach(record => {
          records.push(UserDomainService.IUserToAdminUser(record));
        });

        const response = new ResponseAdminUser(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      });
    });
  }

  async getById(id: string, temporalPassword?: string): Promise<ResponseAdminUser> {
    return new Promise<ResponseAdminUser>( (resolve, reject) => {

      this.userEntityService.findById(id).then(data => {
        const records: AdminUser[] = [];
        records.push(UserDomainService.IUserToAdminUserCreate(data, temporalPassword));

        if (records.length > 0) {
          const response = new ResponseAdminUser(HttpStatus.OK, 'TRANSACCION EXITOSA');
          response.setData(records);
          resolve(response);
        } else {
          const notFound = new ResponseAdminUser(HttpStatus.NOT_FOUND, 'REGISTRO NO ENCONTRADO');
          notFound.setData(records);
          reject(new NotFoundException(notFound));
        }
      });
    });
  }

  async getCatalog(): Promise<ResponseAdminUserCatalog> {
    return new Promise<ResponseAdminUserCatalog>( (resolve, reject) => {

      this.userEntityService.getCatalog().then( data => {
        const records: AdminUserCatalog[] = [];
        data.forEach(record => {
          records.push(UserDomainService.IUserCatalogToAdminUserCatalog(record));
        });

        const response = new ResponseAdminUserCatalog(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setCatalog(records);
        resolve(response);
      })
    });
  }

  async create(requestAdminUser: Partial<RequestAdminUser>): Promise<ResponseAdminUser> {
    const temporalPass = this.getTemporalPassword();
    requestAdminUser.password = await bcrypt.hash(temporalPass, 12);
    const record = UserDomainService.requestAdminUserToUser(requestAdminUser);
    const insertResult =  await this.userEntityService.create(record);
    const recordCreatedId = insertResult.identifiers[0];
    Logger.error(`insertResult ===> ` + insertResult.identifiers[0]);
    Logger.error(`recordCreatedId ===> ` + recordCreatedId);
    return this.getById(recordCreatedId.id, temporalPass);
  }

  getTemporalPassword() {
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < 6; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  async updatePassTemporal(id): Promise<ResponseAdminUser> {
    return new Promise<ResponseAdminUser>(async (resolve, reject) => {

      const recordUpdate: IUser = await this.userEntityService.findById(id);

      if (!recordUpdate) {

        const recordNotFound = new ResponseAdminUser(HttpStatus.NOT_FOUND, 'NO FUE POSIBLE PROCESAR SU SOLICITUD');
        recordNotFound.setData([]);
        reject(new NotFoundException(recordNotFound));
      } else {
        const temporalPass = this.getTemporalPassword();
        recordUpdate.password = await bcrypt.hash(temporalPass, 12);

        await this.userEntityService.updatePass(recordUpdate);

        const response = new ResponseAdminUser(HttpStatus.OK, 'TRANSACCION EXITOSA');
        const records: AdminUser[] = [];
        records.push(UserDomainService.IUserToAdminUserCreate(recordUpdate, temporalPass));
        response.setData(records);
        resolve(response);
      }
    });
  }

  async updatePass(id, request: Partial<RequestAdminChangePassword>): Promise<ResponseAdminUser> {
    return new Promise<ResponseAdminUser>(async (resolve, reject) => {

      const recordUpdate: IUser = await this.userEntityService.findById(id);

      if (!recordUpdate) {

        const recordNotFound = new ResponseAdminUser(HttpStatus.NOT_FOUND, 'NO FUE POSIBLE PROCESAR SU SOLICITUD');
        recordNotFound.setData([]);
        reject(new NotFoundException(recordNotFound));
      } else {

        const resPass = await bcrypt.compare(request.currentPass, recordUpdate.password);

        if (resPass) {
          recordUpdate.password = await bcrypt.hash(request.newPass, 12);
          await this.userEntityService.updatePass(recordUpdate);
          const response = new ResponseAdminUser(HttpStatus.OK, 'TRANSACCION EXITOSA');
          const records: AdminUser[] = [];
          records.push(UserDomainService.IUserToAdminUser(recordUpdate));
          response.setData(records);
          resolve(response);
        } else {
          const notFound = new ResponseAdminUser(HttpStatus.NOT_FOUND, 'ERROR EN TRANSACCION');
          resolve(notFound);
          return notFound;
        }
      }
    });
  }

  async update(requestAdminUser: Partial<RequestAdminUser>): Promise<ResponseAdminUser> {

    return new Promise<ResponseAdminUser>(async (resolve, reject) => {

      const recordUpdate: IUser = await this.userEntityService.findById(requestAdminUser.id);

      if (!recordUpdate) {

        const recordNotFound = new ResponseAdminUser(HttpStatus.NOT_FOUND, 'NO FUE POSIBLE PROCESAR SU SOLICITUD');
        recordNotFound.setData([]);
        reject(new NotFoundException(recordNotFound));
      } else {

        recordUpdate.name = requestAdminUser.name;
        recordUpdate.username = requestAdminUser.username;
        recordUpdate.avatar = requestAdminUser.avatar;
        recordUpdate.email = requestAdminUser.email;
        recordUpdate.status = requestAdminUser.status;

        await this.userEntityService.update(recordUpdate);

        const records: AdminUser[] = [];
        records.push(UserDomainService.IUserToAdminUser(recordUpdate));

        const response = new ResponseAdminUser(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      }
    });
  }

  async updateAvatar(userId: string, avatarURL: string): Promise<ResponseAdminUser> {
    return new Promise<ResponseAdminUser>( async (resolve, reject) => {
      const recordUpdate: IUser = await this.userEntityService.findById(userId);

      if (!recordUpdate) {

        const recordNotFound = new ResponseAdminUser(HttpStatus.NOT_FOUND, 'NO FUE POSIBLE PROCESAR SU SOLICITUD');
        recordNotFound.setData([]);
        reject(new NotFoundException(recordNotFound));
      } else {

        recordUpdate.avatar = avatarURL;

        await this.userEntityService.update(recordUpdate);

        const records: AdminUser[] = [];
        records.push(UserDomainService.IUserToAdminUser(recordUpdate));

        const response = new ResponseAdminUser(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      }

    })
  }
}
