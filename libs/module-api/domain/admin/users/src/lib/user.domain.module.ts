import { UserDomainService } from './services/user.domain.service';
import { Module } from '@nestjs/common';
import { OrmModule } from '@sif/module-api/orm';


const providers = [
  UserDomainService
];

@Module({
  exports: [
    ...providers
  ],
  imports: [
    OrmModule
  ],
  providers: [
    ...providers
  ],
  controllers: []
})
export class UserDomainModule {
}
