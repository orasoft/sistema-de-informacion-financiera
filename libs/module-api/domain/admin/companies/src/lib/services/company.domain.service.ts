import { HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { CompanyEntityService, ICompany } from '@sif/module-api/orm';
import { RequestAdminCompany } from '@sif/shared/message/request';
import {
  AdminCompany,
  AdminCompanyCatalog,
  ResponseAdminCompany,
  ResponseAdminCompanyCatalog
} from '@sif/shared/message/response';

@Injectable()
export class CompanyDomainService {

  constructor(private readonly companyEntityService: CompanyEntityService) {
  };

  static ICompanyToAdminCompany(record: ICompany): AdminCompany {
    return {
      id: record.id,
      address: record.address,
      dui: record.dui,
      logo: record.logo,
      name: record.name,
      nit: record.nit,
      nrc: record.nrc,
      phone: record.phone,
      status: record.status,
      type: record.type,
      version: record.version,
      createAt: record.createAt,
      createBy: record.createBy,
      updatedAt: record.updatedAt,
      updatedBy: record.updatedBy
    };
  }

  static requestAdminCompanyToICompany(requestAdminCompany: Partial<RequestAdminCompany>): Partial<ICompany> {
    return {
      id: requestAdminCompany.id || undefined,
      name: requestAdminCompany.name,
      dui: requestAdminCompany.dui || null,
      address: requestAdminCompany.address || null,
      nrc: requestAdminCompany.nrc || null,
      type: requestAdminCompany.type || null,
      phone: requestAdminCompany.phone || null,
      status: requestAdminCompany.status,
      nit: requestAdminCompany.nit || null
    };
  }

  static ICompanyCatalogToAdminCompanyCatalog(record: ICompany): AdminCompanyCatalog {
    return {
      id: record.id,
      name: record.name
    }
  }

  async getAll(): Promise<ResponseAdminCompany> {
    return new Promise<ResponseAdminCompany>(resolve => {
      this.companyEntityService.findAll().then(data => {

        const records: AdminCompany[] = [];
        data.forEach(record => {
          records.push(CompanyDomainService.ICompanyToAdminCompany(record));
        });

        const response = new ResponseAdminCompany(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      });
    });
  }

  async getById(id: string): Promise<ResponseAdminCompany> {
    return new Promise<ResponseAdminCompany>((resolve, reject) => {

      this.companyEntityService.findById(id).then(data => {
        const records: AdminCompany[] = [];
        records.push(CompanyDomainService.ICompanyToAdminCompany(data));

        if (records.length > 0) {
          const response = new ResponseAdminCompany(HttpStatus.OK, 'TRANSACCION EXITOSA');
          response.setData(records);
          resolve(response);
        } else {
          const notFound = new ResponseAdminCompany(HttpStatus.NOT_FOUND, 'REGISTRO NO ENCONTRADO');
          notFound.setData(records);
          reject(new NotFoundException(notFound));
        }

      });

    });
  }

  async getCatalog(): Promise<ResponseAdminCompanyCatalog> {
    return new Promise<ResponseAdminCompanyCatalog>( (resolve, reject) => {

      this.companyEntityService.getCatalog().then( data => {
        const records: AdminCompanyCatalog[] = [];
        data.forEach(record => {
          records.push(CompanyDomainService.ICompanyCatalogToAdminCompanyCatalog(record));
        });

        const response = new ResponseAdminCompanyCatalog(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setCatalog(records);
        resolve(response);
      })

    });
  }

  async create(requestAdminCompany: Partial<RequestAdminCompany>): Promise<ResponseAdminCompany> {
    const record = CompanyDomainService.requestAdminCompanyToICompany(requestAdminCompany);
    const insertResult = await this.companyEntityService.create(record);
    const recordCreatedId = insertResult.identifiers[0];
    return this.getById(recordCreatedId.id);
  }

  async update(requestAdminCompany: Partial<RequestAdminCompany>): Promise<ResponseAdminCompany> {

    return new Promise<ResponseAdminCompany>(async (resolve, reject) => {

      const recordUpdate: ICompany = await this.companyEntityService.findById(requestAdminCompany.id);

      if (!recordUpdate) {

        const recordNotFound = new ResponseAdminCompany(HttpStatus.NOT_FOUND, 'NO FUE POSIBLE PROCESAR SU SOLICITUD');
        recordNotFound.setData([]);
        reject(new NotFoundException(recordNotFound));

      } else {

        recordUpdate.address = requestAdminCompany.address;
        recordUpdate.dui = requestAdminCompany.dui;
        recordUpdate.name = requestAdminCompany.name;
        recordUpdate.nit = requestAdminCompany.nit;
        recordUpdate.nrc = requestAdminCompany.nrc;
        recordUpdate.phone = requestAdminCompany.phone;
        recordUpdate.status = requestAdminCompany.status;
        recordUpdate.type = requestAdminCompany.type;

        await this.companyEntityService.update(recordUpdate);

        const records: AdminCompany[] = [];
        records.push(CompanyDomainService.ICompanyToAdminCompany(recordUpdate));

        const response = new ResponseAdminCompany(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      }
    });


  }
}
