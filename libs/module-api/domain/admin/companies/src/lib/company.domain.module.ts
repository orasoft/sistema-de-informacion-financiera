import { Module } from '@nestjs/common';
import { CompanyDomainService } from './services/company.domain.service';
import { OrmModule } from '@sif/module-api/orm';

const providers = [
  CompanyDomainService
];

@Module({
  exports: [
    ...providers
  ],
  imports: [
    OrmModule
  ],
  providers: [
    ...providers
  ],
  controllers: []
})
export class CompanyDomainModule {
}
