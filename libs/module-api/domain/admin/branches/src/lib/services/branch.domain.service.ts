import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { AdminBranch, ResponseAdminBranch } from '@sif/shared/message/response';
import { BranchEntityService, CompanyEntityService, IBranch } from '@sif/module-api/orm';
import { RequestAdminBranch } from '@sif/shared/message/request';
import { AdminBranchCatalog } from '@sif/shared/message/response';
import { ResponseAdminBranchCatalog } from '@sif/shared/message/response';


@Injectable()
export class BranchDomainService {

  constructor(private readonly _companyEntityService: CompanyEntityService,
              private readonly _branchEntityService: BranchEntityService) {
  };

  static IBranchToAdminBranch(record: IBranch): AdminBranch {
    return {
      id: record.id,
      name: record.name,
      status: record.status,
      phone: record.phone,
      address: record.address,
      company: record.company,
      version: record.version,
      createAt: record.createAt,
      createBy: record.createBy,
      updatedAt: record.updatedAt,
      updatedBy: record.updatedBy
    };
  }

  static requestAdminBranchToBranchEntity(requestAdminBranch: Partial<RequestAdminBranch>): Partial<IBranch> {
    return {
      id: requestAdminBranch.id || undefined,
      name: requestAdminBranch.name,
      phone: requestAdminBranch.phone,
      address: requestAdminBranch.address,
      status: requestAdminBranch.status
    };
  }

  static IBranchCatalogToAminBranchCatalog(record: IBranch): AdminBranchCatalog {
    return {
      id: record.id,
      name: record.name
    }
  }

  getById(branchId: string): Promise<ResponseAdminBranch> {
    return new Promise<ResponseAdminBranch>(resolve => {
      this._branchEntityService.findById(branchId).then(data => {

        const records: AdminBranch[] = [];
        records.push(BranchDomainService.IBranchToAdminBranch(data));

        const response = new ResponseAdminBranch(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      }, reason => {
        const response = new ResponseAdminBranch(HttpStatus.INTERNAL_SERVER_ERROR, reason.message);
        response.setData([]);
        resolve(response);
      });
    });
  }

  getAllByCompanyId(companyId: string): Promise<ResponseAdminBranch> {
    return new Promise<ResponseAdminBranch>(resolve => {
      this._branchEntityService.findByCompanyId(companyId).then(data => {

        const records: AdminBranch[] = [];
        data.forEach(record => {
          records.push(BranchDomainService.IBranchToAdminBranch(record));
        });

        const response = new ResponseAdminBranch(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      });
    });
  }

  getAllWithCompany(): Promise<ResponseAdminBranch> {
    return new Promise<ResponseAdminBranch>(resolve => {
      this._branchEntityService.getAllWithCompany().then(data => {

        const records: AdminBranch[] = [];
        data.forEach(record => {
          records.push(BranchDomainService.IBranchToAdminBranch(record));
        });

        const response = new ResponseAdminBranch(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      });
    });
  }

  async getCatalog(): Promise<ResponseAdminBranchCatalog> {
    return new Promise<ResponseAdminBranchCatalog>( (resolve, reject) => {
      Logger.log(`Catalog Branch Domain Service`);
      this._branchEntityService.getCatalog().then( data => {
        const records: AdminBranchCatalog[] = [];
        data.forEach( record => {
          records.push(BranchDomainService.IBranchCatalogToAminBranchCatalog(record));
        });

        const response = new ResponseAdminBranchCatalog(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setCatalog(records);
        resolve(response);
      })
    })
  }

  async getCatalogByCompanyId(companyId: string): Promise<ResponseAdminBranchCatalog> {
    return new Promise<ResponseAdminBranchCatalog>( (resolve, reject) => {
      Logger.log(`Catalog Branch Domain Service`);
      this._branchEntityService.getCatalogByCompanyId(companyId).then( data => {
        const records: AdminBranchCatalog[] = [];
        data.forEach( record => {
          records.push(BranchDomainService.IBranchCatalogToAminBranchCatalog(record));
        });

        const response = new ResponseAdminBranchCatalog(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setCatalog(records);
        resolve(response);
      })
    })
  }

  async create(requestAdminBranch: Partial<RequestAdminBranch>) {
    const company = await this._companyEntityService.findById(requestAdminBranch.companyId);
    const record = BranchDomainService.requestAdminBranchToBranchEntity(requestAdminBranch);
    record.company = company;

    const insertResult = await this._branchEntityService.create(record);
    const recordCreatedId = insertResult.identifiers[0].id;
    return this.getById(recordCreatedId);
  }

  async update(requestAdminBranch: Partial<RequestAdminBranch>) {
    const company = await this._companyEntityService.findById(requestAdminBranch.companyId);
    const record = BranchDomainService.requestAdminBranchToBranchEntity(requestAdminBranch);
    record.company = company;
    const updateResult = await this._branchEntityService.update(record);
    const recordUpdatedId = record.id;
    return this.getById(recordUpdatedId);
  }
}
