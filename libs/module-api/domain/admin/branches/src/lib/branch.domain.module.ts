import { Module } from '@nestjs/common';
import { BranchDomainService } from './services/branch.domain.service';
import { OrmModule } from '@sif/module-api/orm';

@Module({
  exports: [
    BranchDomainService
  ],
  imports: [
    OrmModule
  ],
  providers: [
    BranchDomainService
  ],
  controllers: []
})
export class BranchDomainModule {
}
