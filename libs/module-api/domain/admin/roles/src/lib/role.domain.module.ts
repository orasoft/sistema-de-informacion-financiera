import { RoleDomainService } from './services/role.domain.service';
import { OrmModule } from '@sif/module-api/orm';
import { Module } from '@nestjs/common';


const providers = [
  RoleDomainService
];

@Module( {
  exports: [
    ...providers
  ],
  imports: [
    OrmModule
  ],
  providers: [
    ...providers
  ],
  controllers: []
})
export class RoleDomainModule {
}
