import { HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { IRole, RoleEntityService } from '@sif/module-api/orm';
import {
  AdminRole,
  AdminRoleCatalog,
  ResponseAdminRole,
  ResponseAdminRoleCatalog
} from '@sif/shared/message/response';
import { RequestAdminRole, RequestAdminUser } from '@sif/shared/message/request';


@Injectable()
export class RoleDomainService {

  constructor( private readonly roleEntityService: RoleEntityService) {}

  static IRoleToAdminRole(record: IRole): AdminRole {
    return {
      id: record.id,
      name: record.name,
      status: record.status,
      createAt: record.createAt,
      createBy: record.createBy,
      updatedAt: record.updatedAt,
      updatedBy: record.updatedBy,
      version: record.version
    }
  }

  static requestAdminRoleToRole(requestAdminRole: Partial<RequestAdminRole>): Partial<IRole> {
    return {
      id: requestAdminRole.id || undefined,
      name: requestAdminRole.name,
      status: requestAdminRole.status
    }
  }

  static IUserCatalogToAdminUserCatalog(record: IRole): AdminRoleCatalog {
    return {
      id: record.id,
      name: record.name
    }
  }

  async getAll(): Promise<ResponseAdminRole> {
    return new Promise<ResponseAdminRole> ( resolve => {
      this.roleEntityService.findAll().then(data => {
        const records: AdminRole[] = [];
        data.forEach(record => {
          records.push(RoleDomainService.IRoleToAdminRole(record));
        });

        const response = new ResponseAdminRole(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      });
    });
  }

  async getById(id: string): Promise<ResponseAdminRole> {
    return new Promise<ResponseAdminRole>( (resolve, reject) => {

      this.roleEntityService.findById(id).then(data => {
        const records: AdminRole[] = [];
        records.push(RoleDomainService.IRoleToAdminRole(data));

        if (records.length > 0) {
          const response = new ResponseAdminRole(HttpStatus.OK, 'TRANSACCION EXITOSA');
          response.setData(records);
          resolve(response);
        } else {
          const notFound = new ResponseAdminRole(HttpStatus.NOT_FOUND, 'REGISTRO NO ENCONTRADO');
          notFound.setData(records);
          reject(new NotFoundException(notFound));
        }
      });
    });
  }

  async getCatalog(): Promise<ResponseAdminRoleCatalog> {
    return new Promise<ResponseAdminRoleCatalog>( (resolve, reject) => {

      this.roleEntityService.getCatalog().then( data => {
        const records: AdminRoleCatalog[] = [];
        data.forEach(record => {
          records.push(RoleDomainService.IUserCatalogToAdminUserCatalog(record));
        });

        const response = new ResponseAdminRoleCatalog(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setCatalog(records);
        resolve(response);
      })
    });
  }

  async create(requestAdminRole: Partial<RequestAdminRole>): Promise<ResponseAdminRole> {
    const record = RoleDomainService.requestAdminRoleToRole(requestAdminRole);
    const insertResult =  await this.roleEntityService.create(record);
    const recordCreatedId = insertResult.identifiers[0];
    return this.getById(recordCreatedId.id);
  }

  async update(requestAdminRole: Partial<RequestAdminRole>): Promise<ResponseAdminRole> {

    return new Promise<ResponseAdminRole>(async (resolve, reject) => {

      const recordUpdate: IRole = await this.roleEntityService.findById(requestAdminRole.id);

      if (!recordUpdate) {

        const recordNotFound = new ResponseAdminRole(HttpStatus.NOT_FOUND, 'NO FUE POSIBLE PROCESAR SU SOLICITUD');
        recordNotFound.setData([]);
        reject(new NotFoundException(recordNotFound));
      } else {

        recordUpdate.name = requestAdminRole.name;
        recordUpdate.status = requestAdminRole.status;

        await this.roleEntityService.update(recordUpdate);

        const records: AdminRole[] = [];
        records.push(RoleDomainService.IRoleToAdminRole(recordUpdate));

        const response = new ResponseAdminRole(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(records);
        resolve(response);
      }
    });
  }


}
