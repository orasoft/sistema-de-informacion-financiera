import { Module } from '@nestjs/common';
import { LoginDomainService } from './services/login.domain.service';
import { OrmModule } from '@sif/module-api/orm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './services/jwt-strategy';


@Module({
  exports: [
    LoginDomainService
  ],
  imports: [
    OrmModule,
    PassportModule,
    JwtModule.register({
      secret: 'erpSif$2019',
      signOptions: { expiresIn: '3600' },
    }),
  ],
  providers: [
    LoginDomainService,
    JwtStrategy
  ],
  controllers: []
})
export class LoginDomainModule {
}
