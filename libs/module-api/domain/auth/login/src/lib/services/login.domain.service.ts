import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import {
  BranchEntityService,
  CompanyEntityService,
  UserEntityService,
  UserRoleBranchEntityService
} from '@sif/module-api/orm';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { ResponseAuthLogin } from '@sif/shared/message/response';

const jwtDecode = require('jwt-decode');


@Injectable()
export class LoginDomainService {
  constructor(private readonly userEntityService: UserEntityService,
              private readonly branchEntityService: BranchEntityService,
              private readonly userRoleBranchEntityService: UserRoleBranchEntityService,
              private readonly companyEntityService: CompanyEntityService,
              private readonly jwtService: JwtService) {
  }

  async validateUser(username: string, pass: string): Promise<ResponseAuthLogin> {
    const user = await this.userEntityService.findByUsername(username);

    if (user) {
      const resPass = await bcrypt.compare(pass, user.password);
      if (resPass) {
        const response = new ResponseAuthLogin(HttpStatus.OK, 'TRANSACCION EXITOSA');
        response.setData(await this.getToken(user));
        return response;
      } else {
        const notFound = new ResponseAuthLogin(HttpStatus.NOT_FOUND, 'ERROR EN TRANSACCION');
        return notFound;
      }
    }
  }

  async getToken(user: any): Promise<any> {
    Logger.debug(user);
    const payload = { username: user.username, email: user.email };
    Logger.debug(payload);
    Logger.debug(this.jwtService.sign(payload));
    return {
      access_token: await this.jwtService.sign(payload),
      idUser: user.id
    };
  }

  async meCompany(idBranch: string): Promise<any> {
    return await this.branchEntityService.findByIdWithCompany(idBranch)
  }

  async me(idUser: string): Promise<any> {
    const user = await this.userEntityService.findById(idUser);
    let data = null;
    const companies = [];
    const branches = [];
    const roles = [];
    await this.userRoleBranchEntityService.findByUserId(idUser).then(resp => {
      data = resp;
      for (const line of resp) {
        if (roles.length === 0) {
          roles.push({
            id: line.role.id,
            name: line.role.name,
            branchId: line.branch.id
          });
        } else {
          let exist = false;
          roles.forEach(rol => {
            if (rol.id === line.role.id) {
              exist = true;
            }
          });
          if (!exist) {
            roles.push({
              id: line.role.id,
              name: line.role.name,
              branchId: line.branch.id
            });
          }
        }

        if (branches.length === 0) {
          branches.push({
            id: line.branch.id,
            name: line.branch.name
          });
        } else {
          let exist = false;
          branches.forEach(br => {
            if (br.id === line.branch.id) {
              exist = true;
            }
          });
          if (!exist) {
            branches.push({
              id: line.branch.id,
              name: line.branch.name
            });
          }
        }
      }
    });

    // await branches.forEach(br => {
    //   this.branchEntityService.findByIdWithCompany(br.id).then(data2 => {
    //     companies.push({
    //       id: data2.company.id,
    //       name: data2.company.name,
    //       branchId: data2.id
    //     });
    //     Logger.debug({
    //       id: data2.company.id,
    //       name: data2.company.name,
    //       branchId: data2.id
    //     });
    //   });
    // });

    return {
      user: user,
      branches: branches,
      roles: roles
    };
  }
}
