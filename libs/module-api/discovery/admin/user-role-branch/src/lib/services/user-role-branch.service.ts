import { Injectable } from '@nestjs/common';
// tslint:disable-next-line:nx-enforce-module-boundaries
import { UserRoleBranchDomainService } from '../../../../../../domain/admin/user-role-branch/src/lib/services/user-role-branch.domain.service';
import { ResponseAdminUserRoleBranch } from '@sif/shared/message/response';
import { RequestAdminUserRoleBranch } from '@sif/shared/message/request';


@Injectable()
export class UserRoleBranchService {
  constructor(private readonly userRoleBranchDomainService: UserRoleBranchDomainService) {
  }

  async getById(id: string): Promise<ResponseAdminUserRoleBranch> {
    return this.userRoleBranchDomainService.getById(id);
  }

  async getByUserId(userId: string, branchId: string): Promise<ResponseAdminUserRoleBranch> {
    return this.userRoleBranchDomainService.getByUserId(userId, branchId);
  }

  async create(requestAdminUserRoleBranch: Partial<RequestAdminUserRoleBranch>) {
    return this.userRoleBranchDomainService.create(requestAdminUserRoleBranch);
  }

  async delete(id: string): Promise<ResponseAdminUserRoleBranch> {
    return this.userRoleBranchDomainService.delete(id);
  }
}
