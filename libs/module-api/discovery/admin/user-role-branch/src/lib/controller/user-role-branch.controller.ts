import { Controller, Get, Post, Delete, HttpCode, HttpStatus, Param, Body } from '@nestjs/common';
import { UserRoleBranchService } from '../services/user-role-branch.service';
import { ResponseAdminUserRoleBranch } from '@sif/shared/message/response';
import { RequestAdminUserRoleBranch } from '@sif/shared/message/request';


@Controller( {
    path: 'admin/user_role_branch'
})
export class UserRoleBranchController {

  constructor(private readonly userRoleBranchService: UserRoleBranchService){}

  @Get(':id')
  async getById(@Param() params): Promise<ResponseAdminUserRoleBranch> {
    return this.userRoleBranchService.getById(params.id);
  }

  @Get('/user/:userId/:branchId')
  async getByUserId(@Param() params): Promise<ResponseAdminUserRoleBranch> {
    return this.userRoleBranchService.getByUserId(params.userId, params.branchId);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() requestAdminUserRoleBranch: Partial<RequestAdminUserRoleBranch>) {
    return this.userRoleBranchService.create(requestAdminUserRoleBranch);
  }

  @Delete(':id')
  async delete(@Param() params): Promise<ResponseAdminUserRoleBranch> {
    return this.userRoleBranchService.delete(params.id);
  }

}
