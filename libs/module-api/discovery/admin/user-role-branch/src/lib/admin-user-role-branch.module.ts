import { Module } from '@nestjs/common';
import { UserRoleBranchController } from './controller/user-role-branch.controller';
import { UserRoleBranchService } from './services/user-role-branch.service';
import { UserRoleBranchDomainModule } from '@sif/module-api/domain/admin/user-role-branch';


@Module({
  controllers: [
    UserRoleBranchController
  ],
  providers: [
    UserRoleBranchService
  ],
  imports: [
    UserRoleBranchDomainModule
  ],
  exports: []
})
export class AdminUserRoleBranchModule {

}
