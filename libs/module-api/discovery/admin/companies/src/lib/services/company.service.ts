import { HttpStatus, Injectable } from '@nestjs/common';
import { ResponseAdminCompany, ResponseAdminCompanyCatalog } from '@sif/shared/message/response';
import { CompanyDomainService } from '@sif/module-api/domain/admin/companies';
import { RequestAdminCompany } from '@sif/shared/message/request';

@Injectable()
export class CompanyService {

  constructor(private readonly companyDomainService: CompanyDomainService) {
  }

  async getAll(): Promise<ResponseAdminCompany> {
    return this.companyDomainService.getAll();
  }

  async getById(id: string): Promise<ResponseAdminCompany> {
    return this.companyDomainService.getById(id);
  }

  async getCatalog(): Promise<ResponseAdminCompanyCatalog> {
    return this.companyDomainService.getCatalog();
  }

  async create(requestAdminCompany: Partial<RequestAdminCompany>) {
    return this.companyDomainService.create(requestAdminCompany);
  }

  async update(id: string, requestAdminCompany: Partial<RequestAdminCompany>) {
    if (id !== requestAdminCompany.id) {
      const badRequest = new ResponseAdminCompany(HttpStatus.BAD_REQUEST, 'NO FUE POSIBLE PROCESAR SU SOLICITUD');
      badRequest.setData([]);
      return badRequest;
    }
    return this.companyDomainService.update(requestAdminCompany);
  }
}
