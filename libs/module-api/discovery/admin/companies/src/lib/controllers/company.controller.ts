import { Body, Controller, Get, HttpCode, HttpStatus, Logger, Param, Post, Put } from '@nestjs/common';
import { CompanyService } from '../services/company.service';
import { ResponseAdminCompany, ResponseAdminCompanyCatalog } from '@sif/shared/message/response';
import { RequestAdminCompany } from '@sif/shared/message/request';


@Controller({
  path: 'admin/company'
})
export class CompanyController {

  constructor(private readonly companyService: CompanyService) {
  }

  @Get()
  async getAll(): Promise<ResponseAdminCompany> {
    return this.companyService.getAll();
  }

  @Get('catalog')
  async getCatalog(): Promise<ResponseAdminCompanyCatalog> {
    Logger.log(`Catalog Endpoint`);
    return this.companyService.getCatalog();
  }

  @Get(':id')
  async getById(@Param() params): Promise<ResponseAdminCompany> {
    return this.companyService.getById(params.id);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() requestAdminCompany: Partial<RequestAdminCompany>) {
    return this.companyService.create(requestAdminCompany);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  async update(@Param() params, @Body() requestAdminCompany: Partial<RequestAdminCompany>) {
    return this.companyService.update(params.id, requestAdminCompany);
  }
}
