import { Module } from '@nestjs/common';
import { CompanyController } from './controllers/company.controller';
import { CompanyDomainModule } from '@sif/module-api/domain/admin/companies';
import { CompanyService } from './services/company.service';


@Module({
  controllers: [
    CompanyController
  ],
  providers: [
    CompanyService
  ],
  imports: [
    CompanyDomainModule
  ],
  exports: []
})
export class AdminCompanyModule {
}
