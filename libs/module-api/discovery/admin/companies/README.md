# module-api-discovery-admin-companies

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test module-api-discovery-admin-companies` to execute the unit tests via [Jest](https://jestjs.io).
