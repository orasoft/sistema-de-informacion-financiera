import { Module } from '@nestjs/common';
import { BranchController } from './controllers/branch.controller';
import { BranchService } from './services/branch.service';
import { BranchDomainModule } from '@sif/module-api/domain/admin/branches';

@Module({
  controllers: [
    BranchController
  ],
  providers: [
    BranchService
  ],
  imports: [
    BranchDomainModule
  ],
  exports: []
})
export class AdminBranchModule {
}
