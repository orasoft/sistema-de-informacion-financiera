import { Body, Controller, Get, Logger, Param, Post, Put } from '@nestjs/common';
import { BranchService } from '../services/branch.service';
import { ResponseAdminBranch, ResponseAdminBranchCatalog } from '@sif/shared/message/response';
import { RequestAdminBranch } from '@sif/shared/message/request';

@Controller({
  path: 'admin/branch'
})
export class BranchController {

  constructor(private readonly _branchService: BranchService) {
  }

  @Get('catalog')
  async getCatalog(): Promise<ResponseAdminBranchCatalog> {
    Logger.log(`Catalog Branch controller`);
    return this._branchService.getCatalog();
  }

  @Get('catalog/:companyId')
  async getCatalogByCompanyId(@Param() params): Promise<ResponseAdminBranchCatalog> {
    Logger.log(`Catalog Branch controller`);
    return this._branchService.getCatalogByCompanyId(params.companyId);
  }

  @Get('with-company')
  async getAllWithCompany(): Promise<ResponseAdminBranch> {
    return this._branchService.getAllWithCompany();
  }

  @Get(':companyId')
  async getAllByCompanyId(@Param() params): Promise<ResponseAdminBranch> {
    return this._branchService.getAllByCompanyId(params.companyId);
  }

  @Post(':companyId')
  async create(@Param() params, @Body() request: RequestAdminBranch) {
    return this._branchService.create(request);
  }

  @Put(':branchId')
  async update(@Param() params, @Body() request: RequestAdminBranch) {
    return this._branchService.update(request);
  }
}
