import { Injectable, Logger } from '@nestjs/common';
import { BranchDomainService } from '@sif/module-api/domain/admin/branches';
import { ResponseAdminBranch, ResponseAdminBranchCatalog } from '@sif/shared/message/response';
import { RequestAdminBranch } from '@sif/shared/message/request';

@Injectable()
export class BranchService {

  constructor(private readonly _branchDomainService: BranchDomainService) {
  }

  getAllByCompanyId(companyId: string): Promise<ResponseAdminBranch> {
    return this._branchDomainService.getAllByCompanyId(companyId);
  }

  getAllWithCompany(): Promise<ResponseAdminBranch> {
    return this._branchDomainService.getAllWithCompany();
  }

  create(request: RequestAdminBranch) {
    return this._branchDomainService.create(request);
  }

  update(request: RequestAdminBranch) {
    return this._branchDomainService.update(request);
  }

  async getCatalog(): Promise<ResponseAdminBranchCatalog> {
    Logger.log(`Catalog Branch Service`);
    return this._branchDomainService.getCatalog();
  }

  async getCatalogByCompanyId(companyId: string): Promise<ResponseAdminBranchCatalog> {
    Logger.log(`Catalog Branch Service`);
    return this._branchDomainService.getCatalogByCompanyId(companyId);
  }
}
