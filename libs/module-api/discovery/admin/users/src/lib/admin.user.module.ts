import { UserController } from './controller/user.controller';
import { UserService } from './services/user.service';
import { UserDomainModule } from '@sif/module-api/domain/admin/users';
import { Module } from '@nestjs/common';

@Module({
  controllers: [
    UserController
  ],
  providers: [
    UserService
  ],
  imports: [
    UserDomainModule
  ],
  exports: []
})
export class AdminUserModule {}
