import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Logger,
  Param,
  Post,
  Put, Res,
  UploadedFile, UploadedFiles,
  UseInterceptors
} from '@nestjs/common';
import { UserService } from '../services/user.service';
import { AdminUser, ResponseAdminUser, ResponseAdminUserCatalog } from '@sif/shared/message/response';
import { RequestAdminChangePassword, RequestAdminUser } from '@sif/shared/message/request';
import { FileInterceptor } from '@nestjs/platform-express';
import { extname } from 'path';
import { diskStorage } from 'multer';


@Controller({
  path: 'admin/user'
})
export class UserController {
  SERVER_URL  =  "http://localhost:3333/";
  constructor(private readonly userService: UserService){}

  @Get()
  async getAll(): Promise<ResponseAdminUser> {
    return this.userService.getAll();
  }

  @Get('catalog')
  async getCatalog(): Promise<ResponseAdminUserCatalog> {
    Logger.log(`Catalog Endpoint`);
    return this.userService.getCatalog();
  }

  @Get(':id')
  async getById(@Param() params): Promise<ResponseAdminUser> {
    return this.userService.getById(params.id);
  }

  @Get('avatars/:userId')
  async serveAvatar(@Param() params, @Res() res): Promise<any> {
    this.userService.getById(params.userId).then( resp => {
      res.sendFile(resp.data[0].avatar, { root: 'avatars'});
    });
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() requestAdminCompany: Partial<RequestAdminUser>) {
    return this.userService.create(requestAdminCompany);
  }

  @Post(':userId/avatar')
  @UseInterceptors(FileInterceptor('file',
    {
      storage: diskStorage({
        destination: './avatars',
        filename: (req, file, cb) => {
          const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
          return cb(null, `${randomName}${extname(file.originalname)}`)
        }
      })
    }
    )
  )
  async uploadedAvatar(@UploadedFile() file, @Param('userId') userId) {
    console.log(file);
    return this.userService.setAvatar(userId, file.filename);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  async update(@Param() params, @Body() requestAdminCompany: Partial<RequestAdminUser>) {
    return this.userService.update(params.id, requestAdminCompany);
  }

  @Put('resetpass/:id')
  @HttpCode(HttpStatus.OK)
  async updatePassTemporal(@Param() params) {
    return this.userService.updatePassTemporal(params.id);
  }

  @Put('changePass/:id')
  @HttpCode(HttpStatus.OK)
  async updatePass(@Param() params, @Body() request: Partial<RequestAdminChangePassword>) {
    return this.userService.updatePass(params.id, request);
  }
}
