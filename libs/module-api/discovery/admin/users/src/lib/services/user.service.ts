import { HttpStatus, Injectable } from '@nestjs/common';
import { UserDomainService } from '@sif/module-api/domain/admin/users';
import { ResponseAdminUser, ResponseAdminUserCatalog } from '@sif/shared/message/response';
import { RequestAdminChangePassword, RequestAdminUser } from '@sif/shared/message/request';

@Injectable()
export class UserService {
  constructor(private readonly userDomainService: UserDomainService){
  }

  async getAll(): Promise<ResponseAdminUser> {
    return this.userDomainService.getAll();
  }

  async getById(id: string): Promise<ResponseAdminUser> {
    return this.userDomainService.getById(id);
  }

  async getCatalog(): Promise<ResponseAdminUserCatalog> {
    return this.userDomainService.getCatalog();
  }

  async create(requestAdminUser: Partial<RequestAdminUser>) {
    return this.userDomainService.create(requestAdminUser);
  }

  async update(id: string, requestAdminUser: Partial<RequestAdminUser>) {
    if (id !== requestAdminUser.id) {
      const badRequest = new ResponseAdminUser(HttpStatus.BAD_REQUEST, 'NO FUE POSIBLE PROCESAR SU SOLICITUD');
      badRequest.setData([]);
      return badRequest;
    }
    return this.userDomainService.update(requestAdminUser);
  }

  async updatePassTemporal(id: string) {
    return this.userDomainService.updatePassTemporal(id);
  }

  async updatePass(id: string, request: Partial<RequestAdminChangePassword>) {
    return this.userDomainService.updatePass(id, request);
  }

  async setAvatar(userId: string, avatarUrl: string) {
    return this.userDomainService.updateAvatar(userId, avatarUrl);
  }
}
