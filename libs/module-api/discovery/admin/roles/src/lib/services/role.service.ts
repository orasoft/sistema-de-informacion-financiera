import { HttpStatus, Injectable } from '@nestjs/common';
import { RoleDomainService } from '../../../../../../domain/admin/roles/src/lib/services/role.domain.service';
import { ResponseAdminRole, ResponseAdminRoleCatalog } from '@sif/shared/message/response';
import { RequestAdminRole } from '@sif/shared/message/request';


@Injectable()
export class RoleService {

  constructor(private readonly roleDomainService: RoleDomainService) {
  }

  async getAll(): Promise<ResponseAdminRole> {
    return this.roleDomainService.getAll();
  }

  async getById(id: string): Promise<ResponseAdminRole> {
    return this.roleDomainService.getById(id);
  }

  async getCatalog(): Promise<ResponseAdminRoleCatalog> {
    return this.roleDomainService.getCatalog();
  }

  async create(requestAdminRole: Partial<RequestAdminRole>) {
    return this.roleDomainService.create(requestAdminRole);
  }

  async update(id: string, requestAdminRole: Partial<RequestAdminRole>) {
    if (id !== requestAdminRole.id) {
      const badRequest = new ResponseAdminRole(HttpStatus.BAD_REQUEST, 'NO FUE POSIBLE PROCESAR SU SOLICITUD');
      badRequest.setData([]);
      return badRequest;
    }
    return this.roleDomainService.update(requestAdminRole);
  }

}
