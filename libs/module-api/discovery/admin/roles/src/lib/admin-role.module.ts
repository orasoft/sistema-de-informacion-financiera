import { RoleController } from './controllers/role.controller';
import { RoleService } from './services/role.service';
import { RoleDomainModule } from '@sif/module-api/domain/admin/roles';
import { Module } from '@nestjs/common';

@Module({
  controllers: [
    RoleController
  ],
  providers: [
    RoleService
  ],
  imports: [
    RoleDomainModule
  ],
  exports: []
})
export class AdminRoleModule {
}
