import { Body, Controller, Get, HttpCode, HttpStatus, Logger, Param, Post, Put } from '@nestjs/common';
import { RoleService } from '../services/role.service';
import { ResponseAdminRole, ResponseAdminRoleCatalog } from '@sif/shared/message/response';
import { RequestAdminRole } from '@sif/shared/message/request';

@Controller({
  path: 'admin/role'
})
export class RoleController {

  constructor(private readonly roleService: RoleService){}

  @Get()
  async getAll(): Promise<ResponseAdminRole> {
    return this.roleService.getAll();
  }

  @Get('catalog')
  async getCatalog(): Promise<ResponseAdminRoleCatalog> {
    Logger.log(`Catalog Endpoint`);
    return this.roleService.getCatalog();
  }

  @Get(':id')
  async getById(@Param() params): Promise<ResponseAdminRole> {
    return this.roleService.getById(params.id);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() requestAdminRole: Partial<RequestAdminRole>) {
    return this.roleService.create(requestAdminRole);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  async update(@Param() params, @Body() requestAdminRole: Partial<RequestAdminRole>) {
    return this.roleService.update(params.id, requestAdminRole);
  }

}
