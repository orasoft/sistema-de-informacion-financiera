import { Body, Controller, Get, HttpCode, HttpStatus, Logger, Post, Param } from '@nestjs/common';
import { LoginService } from '../services/login.service';


@Controller({
  path: 'auth/login'
})
export class LoginController {

  constructor(private readonly loginService: LoginService) {}

  @Get('me/:idUser')
  async me(@Param() params) {
    return this.loginService.me(params.idUser);
  }

  @Get('company/:idBranch')
  async meCompany(@Param() params) {
    return this.loginService.meCompany(params.idBranch);
  }

  @Post()
  @HttpCode(HttpStatus.OK)
  async login(@Body() user) {
    return this.loginService.login(user);
  }

}
