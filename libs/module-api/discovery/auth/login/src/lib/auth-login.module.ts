import { Module } from '@nestjs/common';
import { LoginController } from './controller/login.controller';
import { LoginService } from './services/login.service';
import { LoginDomainModule } from '@sif/module-api/domain/auth/login';

@Module({
  controllers: [
    LoginController
  ],
  providers: [
    LoginService
  ],
  imports: [
    LoginDomainModule
  ],
  exports: []
})
export class AuthLoginModule {}
