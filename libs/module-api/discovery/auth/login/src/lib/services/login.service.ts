import { Injectable } from '@nestjs/common';
// tslint:disable-next-line:nx-enforce-module-boundaries
import { LoginDomainService } from '../../../../../../domain/auth/login/src/lib/services/login.domain.service';


@Injectable()
export class LoginService{
  constructor(private readonly loginDomainService: LoginDomainService){}

  async login(user: any): Promise<any> {
    return this.loginDomainService.validateUser(user.username, user.password);
  }

  async me(idUser: string): Promise<any> {
    return this.loginDomainService.me(idUser);
  }

  async meCompany(idBranch: string): Promise<any> {
    return this.loginDomainService.meCompany(idBranch);
  }
}
